const functions = require("firebase-functions");
const admin = require("firebase-admin");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const Twitter = require("twitter");
const QuickChart = require("quickchart-js");
const axios = require("axios");
const path = require("path");
const os = require("os");
const fs = require("fs");
const got = require("got");
const FileType = require("file-type");

const db = admin.database();
const activeSpeakerRef = db.ref("data/activeSpeakers");
const bucketName = "gs://zoombot.appspot.com";
const twitterUid = "zoom:jy7ObDlXTqC7AdBwluma1A"; // ##### Update with Patrick's UID #####
const interventionUser = "Peter Chen"; // ##### Update with Patrick's User Name #####

const client = new Twitter({
  consumer_key: environment.twitter.consumer_key,
  consumer_secret: environment.twitter.consumer_secret,
  access_token_key: environment.twitter.access_token_key,
  access_token_secret: environment.twitter.access_token_secret,
});

let pieData = {
  type: "outlabeledPie",
  data: {
    labels: [],
    datasets: [
      {
        backgroundColor: [],
        data: [],
      },
    ],
  },
  options: {
    plugins: {
      legend: false,
      outlabels: {
        text: "%l %p",
        color: "white",
        stretch: 25,
        font: {
          resizable: true,
          minSize: 6,
          maxSize: 6,
        },
      },
    },
  },
};

exports.createTwitter = functions.database
  .ref("/scheduling/{ip}/sensors")
  .onDelete(async (snap, context) => {
    const keyVal = Object.keys(snap.val())[0];
    const sensorKey = Object.keys(snap.val())[0].split("-");
    let uid = snap.val()[keyVal].uid;
    let meetingNo = sensorKey[0];
    let sensorId = `ZoomSensor_${sensorKey[1]}`;

    // functions.logger.info("Meeting No: ", meetingNo);
    // functions.logger.info("Sensor Id: ", sensorId);
    // functions.logger.info("UID: ", uid);

    if (uid !== twitterUid || sensorId !== "ZoomSensor_1") return;
    else {
      const activeSpeakerSnap = await activeSpeakerRef
        .child(`${meetingNo}/${sensorId}/history`)
        .once("value");

      let index = 0;
      activeSpeakerSnap.forEach((snapshot) => {
        const events = Object.values(snapshot.val().activeHistory);
        const users = snapshot.val().userList;
        // functions.logger.info("Events: ", events);
        // functions.logger.info("Users: ", users);

        if (
          events &&
          events.length > 0 &&
          users &&
          Object.keys(users).length > 0
        ) {
          const userDict = getUserDict(events);
          const userIds = Object.keys(userDict);
          // functions.logger.info("User Dict: ", userDict);

          for (let userId of userIds) {
            let userName = users[userId] ? users[userId].username : "Silence";

            if (!pieData.data.labels.includes(userName)) {
              pieData.data.labels.push(userName);
              if (userName !== "Silence")
                pieData.data.datasets[0].backgroundColor.push(
                  `hsla(${Math.floor((index * 80) % 360)},60%,60%,1)`
                );
              else
                pieData.data.datasets[0].backgroundColor.push(
                  `hsla(0,0%,70%,1)`
                );
              pieData.data.datasets[0].data.push(userDict[userId] / 1000 / 60);
              index++;
            } else {
              let labelIndex = pieData.data.labels.indexOf(userName);
              pieData.data.datasets[0].data[labelIndex] +=
                userDict[userId] / 1000 / 60;
            }
          }
        }
      });

      let userIndex = 1;
      for (let i = 0; i < pieData.data.labels.length; i++) {
        if (
          pieData.data.labels[i] !== "Silence" &&
          pieData.data.labels[i] !== interventionUser
        ) {
          pieData.data.labels[i] = `U${userIndex}`;
          userIndex++;
        }
      }

      // functions.logger.info("Pie Data: ", pieData);

      const myChart = new QuickChart();
      myChart
        .setConfig(pieData)
        .setWidth(400)
        .setHeight(400)
        .setBackgroundColor("transparent");

      const chartUrl = myChart.getUrl();
      functions.logger.info("Chart Url: ", chartUrl);

      try {
        // Step 1: Download mediaUrl to temporary directory
        let tmpDownloadedFile = await downloadRemoteUrlImage(
          chartUrl,
          meetingNo
        );
        functions.logger.info(
          "Download Complete: File Path: " +
            tmpDownloadedFile.filePath +
            " - File Name: ",
          tmpDownloadedFile.fileName
        );

        // Step 2: Upload to Firestore Storage
        await uploadLocalFileToStorage(
          tmpDownloadedFile.filePath,
          tmpDownloadedFile.fileName
        );

        // Step 3: Post to Twitter
        const data = require("fs").readFileSync(tmpDownloadedFile.filePath);
        client.post(
          "media/upload",
          { media: data },
          (error, media, response) => {
            if (!error) {
              var status = {
                status: "ZoomSense Behaviour Change Intervention",
                media_ids: media.media_id_string, // Pass the media id string
              };

              client.post(
                "statuses/update",
                status,
                (error, tweet, response) => {
                  if (error)
                    functions.logger.info(
                      "Error on creating twitter: " + error
                    );
                }
              );
            }
          }
        );
      } catch (e) {
        functions.logger.info(
          "Error on downloading and uploading remote media file to storage: " +
            e
        );
      }
    }
  });

function getUserDict(events) {
  let userDict = {};

  for (let i = 0; i < events.length - 1; i++) {
    if (typeof events[i].zoomid === "number") {
      let uu = events[i].zoomid;
      if (!(uu in userDict)) userDict[uu] = 0;

      userDict[uu] += events[i + 1].timestamp - events[i].timestamp;
      continue;
    }

    if (Array.isArray(events[i].zoomid) && events[i].zoomid.length) {
      for (let j = 0; j < events[i].zoomid.length; j++) {
        let thisUser = events[i].zoomid[j];
        if (!userDict[thisUser]) userDict[thisUser] = 0;

        userDict[thisUser] += events[i + 1].timestamp - events[i].timestamp;
      }
    } else {
      if (!userDict["__Silence"]) userDict["__Silence"] = 0;

      userDict["__Silence"] += events[i + 1].timestamp - events[i].timestamp;
    }
  }

  return userDict;
}

/**
 *
 * @param {String} fileUrl
 * @param {String} fileName
 */
async function downloadRemoteUrlImage(fileUrl, fileName) {
  // Identify the remote file type
  let fileExt = "";
  const fileType = await retrieveStreamFileType(fileUrl);
  if (fileType) fileExt = "." + fileType.ext;

  const fileNameWithExt = fileName + fileExt;
  const tempFilePath = path.join(os.tmpdir(), fileNameWithExt);
  functions.logger.info("Temp File Path: " + tempFilePath);

  const writer = fs.createWriteStream(tempFilePath);

  return axios({
    method: "get",
    url: fileUrl,
    responseType: "stream",
  }).then((response) => {
    // Wait for another promise to write the file completely into disk
    return new Promise((resolve, reject) => {
      response.data.pipe(writer);
      let error = null;
      writer.on("error", (err) => {
        error = err;
        writer.close();
        throw new Error(err);
      });
      writer.on("close", () => {
        if (!error) {
          resolve({
            filePath: tempFilePath,
            fileName: fileNameWithExt,
          });
        }
      });
    });
  });
}

/**
 * Retrieve the file type .jpg, .png, etc.
 * @param {String} fileUrl
 */
async function retrieveStreamFileType(fileUrl) {
  const stream = got.stream(fileUrl);

  try {
    const fileType = await FileType.fromStream(stream);
    return fileType;
  } catch (e) {
    functions.logger.error("error trying to identify remote file type: ", e);
    return null;
  }
}

/**
 * Upload the file in firestore storage
 * @param {String} filePath
 * @param {String} fileName
 */
async function uploadLocalFileToStorage(filePath, fileName) {
  const imageBucket = "images/";

  const bucket = admin.storage().bucket(bucketName);
  const destination = `${imageBucket}${fileName}`;

  try {
    // Uploads a local file to the bucket
    await bucket.upload(filePath, {
      destination: destination,
      gzip: true,
      metadata: {
        cacheControl: "public, max-age=31536000",
      },
    });

    functions.logger.info(
      `${fileName} uploaded to /${imageBucket}/${fileName}.`
    );
  } catch (e) {
    throw new Error("uploadLocalFileToStorage failed: " + e);
  }
}
