const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { getMsDiff, isOverlapping } = require("../utils/timeformatter");
const db = admin.database();
const activeSpeakerRef = db.ref("data/activeSpeakers");
const meetingRef = db.ref("meetings");
const reallocationRef = db.ref("reallocation");
const whitelistRef = db.ref("whitelist");
const schedulingRef = db.ref("scheduling");
const chatRef = db.ref("data/chats");
const bucketName = "gs://zoombot.appspot.com";
const bucket = admin.storage().bucket(bucketName);

// Firebase Cloud Function scheduled at a 1-minute interval
exports.zoomScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async () => {
    try {
      // Retrieve whitelisted Uids
      const whitelistSnap = await whitelistRef.once("value");
      const whitelist = await getWhitelistIds(whitelistSnap.val());

      // Retrieve all meetings need to be scheduled
      const meetingSnap = await meetingRef.once("value");
      const meetings = meetingSnap.val();
      const scheduleMeetings = await getScheduleMeetings(meetings, whitelist);

      // Perform scheduling for meetings which has started for more than 30 seconds
      const schedulingSnap = await schedulingRef.once("value");
      const scheduling = schedulingSnap.val();
      await pushSchedules(scheduling, scheduleMeetings);
    } catch (error) {
      functions.logger.error(error);
    }
  });

// Firebase Cloud Function scheduled at a 1 minute interval to send leave
// meeting commands to ZoomSensors when the meeting end time is before
// the current time
exports.leaveMeetingScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async () => {
    try {
      // Get all currently running sensors
      const schedulingSnap = await schedulingRef.once("value");
      const scheduling = schedulingSnap.val();
      const keys = Object.keys(scheduling);

      // Check all vms currently running
      const references = [];
      const promises = [];
      for (let i = 0; i < keys.length; i++) {
        const vm = scheduling[keys[i]];
        const sensors = vm.sensors;

        if (sensors) {
          const sensorKeys = Object.keys(sensors);

          for (let j = 0; j < sensorKeys.length; j++) {
            const currentSensor = sensors[sensorKeys[j]];
            const uid = currentSensor.uid;
            const meetingNo = sensorKeys[j].split("-")[0];
            references.push(`${keys[i]}-${sensorKeys[j]}`);
            promises.push(
              meetingRef.child(`${uid}/${meetingNo}`).once("value")
            );
          }
        }
      }

      const results = await Promise.all(promises);

      const leavePromises = [];
      for (let i = 0; i < results.length; i++) {
        const result = results[i].val();
        const endTime = result.endTime;
        const diff = getMsDiff(endTime);
        const ip = references[i].split("-")[0];
        const meetingNo = references[i].split("-")[1];
        const sensorId = `ZoomSensor_${references[i].split("-")[2]}`;

        functions.logger.info(`${meetingNo}-${sensorId} Diff: ${diff}`);

        // If end time is before the current time, set the leave cmd to under the chat node
        if (diff < 0) {
          leavePromises.push(
            schedulingRef
              .child(`${ip}/leaving/${meetingNo}-${sensorId}`)
              .set(true)
          );

          leavePromises.push(
            chatRef
              .child(`${meetingNo}/${sensorId}/leave`)
              .update({ leave: "LEAVE_MEETING" })
          );
        }
      }
      await Promise.all(leavePromises);
    } catch (error) {
      functions.logger.error(error);
    }
  });

/**
 * Get all whitelisted uids from Firebase
 * @param {*} whitelist Whitelist snapshot in Firebase
 */
async function getWhitelistIds(whitelist) {
  const allUsers = await admin.auth().listUsers();

  const userList = [];
  for (let i = 0; i < allUsers.users.length; i++) {
    const user = allUsers.users[i];
    userList[user.email] = user.uid;
  }

  const whitelistUids = [];
  const userListKeys = Object.keys(userList);
  userListKeys.forEach((userKey) => {
    if (whitelist.includes(userKey)) whitelistUids.push(userList[userKey]);
  });

  return whitelistUids;
}

/**
 * Get meetings which need to be scheduled
 * @param {*} meetings Meeting snapshot in Firebase
 * @param {*} whitelist Whitelist snapshot in Firebase
 */
async function getScheduleMeetings(meetings, whitelist) {
  const uidKeys = Object.keys(meetings);
  const promises = [];
  const scheduleMeetings = [];

  // Loop through all uids under the meeting node
  uidKeys.forEach((uid) => {
    const uidMeetings = meetings[uid];
    const meetingKeys = Object.keys(uidMeetings);

    // If the uid is not in the whitelist, skip scheduling
    if (!whitelist.includes(uid)) {
      meetingKeys.forEach((meetingKey) => {
        const meeting = uidMeetings[meetingKey];
        if (meeting.type !== "Vonage")
          promises.push(
            meetingRef
              .child(`${uid}/${meetingKey}`)
              .update({ scheduled: false })
          );
      });
    }
    // Otherwise, calculate the time differences between the current time and schedule time.
    // If the meeting started more than 30 seconds in the past, and has not already ended,
    // then add to the scheduling meeting list
    else {
      meetingKeys.forEach((meetingKey) => {
        const meeting = uidMeetings[meetingKey];
        meeting.meetingNo = meetingKey;
        meeting.uid = uid;
        const { scheduled, startTime, endTime } = meeting;

        if (startTime) {
          const startTimeDiffMs = getMsDiff(startTime);
          const endTimeDiffMs = getMsDiff(endTime);
          // Schedule meeting whose start time is more than 30 seconds in the past, that hasn't already ended
          if (scheduled === undefined && startTimeDiffMs < -30 * 1000 && endTimeDiffMs > 0) {
            functions.logger.info(
              `Start Zoom Sense for ${meetingKey} at ${startTime}`
            );
            promises.push(
              meetingRef
                .child(`${uid}/${meetingKey}`)
                .update({ scheduled: true })
            );
            scheduleMeetings.push(meeting);
          }
        } else {
          promises.push(
            meetingRef
              .child(`${uid}/${meetingKey}`)
              .update({ error: "Missing start time of the meeting" })
          );
        }
      });
    }
  });

  await Promise.all(promises);
  return scheduleMeetings;
}

/**
 * Push schedulings to Firebase based on the resource availability
 * @param {*} scheduling Scheduling snapshot in Firebase
 * @param {*} scheduleMeetings Meeting list which need to be scheduled
 */
async function pushSchedules(scheduling, scheduleMeetings) {
  let tempScheduling = scheduling;
  const keys = Object.keys(tempScheduling);
  const promises = [];

  // Loop through all the meetings need to be scheduled
  for (let i = 0; i < scheduleMeetings.length; i++) {
    const meetingToSchedule = scheduleMeetings[i];
    const noBo = meetingToSchedule.noOfBreakoutRooms;
    let allocateIndex = 0;

    // Check all vms currently running
    for (let j = 0; j < keys.length; j++) {
      const vm = tempScheduling[keys[j]];
      const capacity = vm.capacity;
      const sensors = vm.sensors;

      // Get the number of sensors currently running
      let inUse = !sensors ? 0 : Object.keys(sensors).length;
      if (inUse === 0) tempScheduling[keys[j]].sensors = [];

      // Allocate sensors if the vm still has capacity
      const startIndex = allocateIndex;
      for (let k = startIndex; k < noBo; k++) {
        if (inUse < capacity) {
          const sensorIndex = `${meetingToSchedule.meetingNo}-${k + 1}`;
          const key = `${keys[j]}/sensors/${sensorIndex}`;
          const value = {
            startTime: meetingToSchedule.startTime,
            uid: meetingToSchedule.uid,
            password: meetingToSchedule.password,
          };
          if (meetingToSchedule.webinarToken)
            value.webinarToken = meetingToSchedule.webinarToken;
          promises.push(schedulingRef.child(key).set(value));

          // Record VM Allocation under the meeting node
          promises.push(
            meetingRef
              .child(
                `${meetingToSchedule.uid}/${
                  meetingToSchedule.meetingNo
                }/vmAllocation/ZoomSensor_${k + 1}`
              )
              .set(keys[j])
          );
          inUse++;
          allocateIndex++;
          tempScheduling[keys[j]].sensors[key] = value;
        } else break;
      }

      // Stop allocation for the current meeting if all bos have been assigned
      if (allocateIndex === noBo) break;
    }

    // If not enough resources available, set unallocated number to the meeting node
    if (allocateIndex < noBo) {
      let totalEndingMeetings = 0;
      let unallocated = noBo - allocateIndex;
      const leavingRefs = [];
      const ipRefs = [];

      for (let i = 0; i < keys.length; i++) {
        const vm = scheduling[keys[i]];
        const leaving = vm.leaving;
        if (leaving) {
          const leavingKeys = Object.keys(leaving);
          totalEndingMeetings += leavingKeys.length;
          for (let j = 0; j < leavingKeys.length; j++) {
            leavingRefs.push(leavingKeys[j]);
            ipRefs.push(keys[i]);
          }
        }
      }

      functions.logger.info("unallocated: ", unallocated);
      functions.logger.info("totalEndingMeetings: ", totalEndingMeetings);

      if (totalEndingMeetings > 0) {
        for (
          let i = allocateIndex;
          i < allocateIndex + Math.min(unallocated, totalEndingMeetings);
          i++
        ) {
          const key = leavingRefs[i];
          const value = {
            startTime: meetingToSchedule.startTime,
            uid: meetingToSchedule.uid,
            password: meetingToSchedule.password,
            meetingNo: meetingToSchedule.meetingNo,
            sensorIdx: i + 1,
          };
          if (meetingToSchedule.webinarToken)
            value.webinarToken = meetingToSchedule.webinarToken;
          promises.push(reallocationRef.child(key).set(value));

          promises.push(
            meetingRef
              .child(
                `${meetingToSchedule.uid}/${
                  meetingToSchedule.meetingNo
                }/vmAllocation/ZoomSensor_${i + 1}`
              )
              .set(ipRefs[i])
          );
        }
      }

      unallocated = unallocated - totalEndingMeetings;
      if (unallocated > 0) {
        promises.push(
          meetingRef
            .child(
              `${meetingToSchedule.uid}/${meetingToSchedule.meetingNo}/unallocated`
            )
            .set(unallocated - totalEndingMeetings)
        );
      }
    }
  }

  await Promise.all(promises);
}

exports.sanityCheck = functions.database
  .ref("/scheduling/{ip}/sensors")
  .onDelete(async (snap) => {
    const keyVal = Object.keys(snap.val())[0];
    const sensorKey = Object.keys(snap.val())[0].split("-");
    let uid = snap.val()[keyVal].uid;
    let meetingNo = sensorKey[0];
    let sensorId = `ZoomSensor_${sensorKey[1]}`;

    functions.logger.info("Meeting No: ", meetingNo);
    functions.logger.info("Sensor Id: ", sensorId);
    functions.logger.info("UID: ", uid);

    const activeSpeakerSnap = await activeSpeakerRef
      .child(`${meetingNo}/${sensorId}/history`)
      .once("value");

    // Loop through the history nodes and check the recording flags
    const isRecordingPromises = [];
    const hasRecordingPromises = [];
    const timestampList = [];
    activeSpeakerSnap.forEach((snapshot) => {
      const timestamp = snapshot.key;
      let isRecording = snapshot.val().isRecording;
      let hasRecording = snapshot.val().hasRecording;

      // Update isRecording to be false
      if (isRecording)
        isRecordingPromises.push(
          activeSpeakerRef
            .child(`${meetingNo}/${sensorId}/history/${timestamp}/isRecording`)
            .set(false)
        );

      // Check for those without hasRecording flag
      if (!hasRecording) {
        const file = bucket.file(
          `${uid}/${meetingNo}/${sensorId}-${timestamp}/double_click_to_convert_01.meetingrec`
        );
        timestampList.push(timestamp);
        hasRecordingPromises.push(file.exists());
      }
    });

    await Promise.all(isRecordingPromises);
    const results = await Promise.all(hasRecordingPromises);

    // Update all hasRecording flag
    const promises = [];
    for (let i = 0; i < timestampList.length; i++) {
      const hasRecordingRef = activeSpeakerRef.child(
        `${meetingNo}/${sensorId}/history/${timestampList[i]}/hasRecording`
      );
      if (results[i][0] === true) promises.push(hasRecordingRef.set(true));
      else promises.push(hasRecordingRef.set(false));
    }

    return await Promise.all(promises);
  });

exports.rescheduleSensors = functions.database
  .ref("/scheduling/{ip}/sensors/{sensorId}")
  .onDelete(async (snap, context) => {
    const sensorKey = context.params.sensorId.split("-");
    let meetingNo = sensorKey[0];
    let sensorId = `ZoomSensor_${sensorKey[1]}`;

    functions.logger.info("Meeting No: ", meetingNo);
    functions.logger.info("Sensor Id: ", sensorId);

    const key = `${meetingNo}-${sensorId}`;
    await schedulingRef.child(`${context.params.ip}/leaving/${key}`).remove();
    const reallocationSnap = await reallocationRef.child(key).once("value");
    const reallocation = reallocationSnap.val();

    if (reallocation) {
      const sensorIndex = `${reallocation.meetingNo}-${reallocation.sensorIdx}`;
      const value = {
        startTime: reallocation.startTime,
        uid: reallocation.uid,
        password: reallocation.password,
      };
      if (reallocation.webinarToken)
        value.webinarToken = reallocation.webinarToken;

      await reallocationRef.child(key).remove();
      await schedulingRef
        .child(`${context.params.ip}/sensors/${sensorIndex}`)
        .set(value);
    }
  });

/**
 * Get available capacity when scheduling a ZoomSense meeting which loops through all
 * the pre-scheduled meetings and check any overlapping meetings to determine the actual
 * availability
 *
 * @param {moment.MomentInput} startTime Meeting start time
 * @param {number} duration Meeting Duration in minutes
 *
 * @returns Available capacity for the ZoomSense infrastructure
 */
async function getAvailableCapacity(startTime, duration) {
  try {
    // Retrieve whitelisted UIDs
    const whitelistSnap = await whitelistRef.once("value");
    const whitelist = await getWhitelistIds(whitelistSnap.val());

    // Retrieve all meetings need to be scheduled
    const meetingSnap = await meetingRef.once("value");
    const meetings = meetingSnap.val();
    let overLapping = 0;

    // Loop through all UIDs under the meeting node
    const uidKeys = Object.keys(meetings);
    uidKeys.forEach((uid) => {
      const uidMeetings = meetings[uid];
      const meetingKeys = Object.keys(uidMeetings);

      // Loop through only the whitelisted users
      if (whitelist.includes(uid)) {
        meetingKeys.forEach((meetingKey) => {
          const meeting = uidMeetings[meetingKey];
          const sTime = meeting.startTime;
          const eTime = meeting.endTime;
          if (sTime && eTime) {
            if (isOverlapping(startTime, duration, sTime, eTime)) {
              overLapping += parseInt(meeting.noOfBreakoutRooms);
            }
          }
        });
      }
    });

    // Get current ZoomSense capacity
    const schedulingSnap = await schedulingRef.once("value");
    const scheduling = schedulingSnap.val();
    const keys = Object.keys(scheduling);

    // Check all vms currently running
    let totalCapacity = 0;
    for (let i = 0; i < keys.length; i++) {
      const vm = scheduling[keys[i]];
      const capacity = vm.capacity;
      totalCapacity += capacity;
    }

    functions.logger.info("totalCapacity: ", totalCapacity);
    functions.logger.info("overLapping: ", overLapping);
    return totalCapacity - overLapping;
  } catch (error) {
    functions.logger.error("getAvailableCapacity Error: ", error);
    return undefined;
  }
}

exports.getAvailableCapacity = getAvailableCapacity;
