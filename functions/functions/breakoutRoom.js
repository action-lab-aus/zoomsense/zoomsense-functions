const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();

// Listens for new chat messages added and verify whether leave command need to be pushed
exports.onNewHistoryCreate = functions.database
  .ref("/data/activeSpeakers/{meetingid}/{sensorid}/history/{historyId}")
  .onCreate(async (snapshot, context) => {
    const data = snapshot.val();
    const isInBO = data.isInBO === undefined ? "null" : data.isInBO;

    const { meetingid, sensorid, historyId } = context.params;
    await db
      .ref(`data/logs/boMeta/${meetingid}/${sensorid}/history/${historyId}`)
      .set({ isInBO });
  });

exports.onChatIsInBoUpdate = functions.database
  .ref("/data/chats/{meetingid}/{sensorid}/isInBO")
  .onWrite(async (change, context) => {
    const { meetingid, sensorid } = context.params;
    await db
      .ref(`data/logs/boMeta/${meetingid}/${sensorid}/current`)
      .set({ isInBO: change.after.val() });
  });
