const functions = require("firebase-functions");
const admin = require("firebase-admin");

const db = admin.database();
const meetingRef = db.ref("meetings");
const activeSpeakerRef = db.ref("data/activeSpeakers");
const chatRef = db.ref("data/chats");
const docRef = db.ref("data/gdocs");

// When calculating the speaking percentage for each attendee
// in a Breakout Room, we look `N` minutes back. This
// value is the default for meetings that don't define
// a custom value
const DEFAULT_DATA_LOOKBACK_WINDOW = 5;
// The default frequency in minutes that we send
// prompts out at.
const DEFAULT_PROMPT_FREQUENCY = 10;

/** @returns {number} */
const getPromptFrequencySafe = (meeting) =>
  ((meeting.modules || {}).prompts || {}).promptFrequency ||
  DEFAULT_PROMPT_FREQUENCY;

/** @returns {number} */
const getDataLookbackWindowSafe = (meeting) =>
  ((meeting.modules || {}).prompts || {}).dataLookbackWindow ||
  DEFAULT_PROMPT_FREQUENCY;

// Firebase Cloud Function scheduled at a 1 minute interval to check whether high-level
// prompt messages need to be sent or not
exports.promptScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async (context) => {
    try {
      // Retrieve all meetings need to be scheduled
      const meetingSnap = await meetingRef.once("value");
      const meetings = meetingSnap.val();
      const runningMeetings = getRunningMeetings(meetings);

      // Filter out those meetings that don't have the prompts
      // module enabled
      const meetingsToPrompt = runningMeetings.filter(
        (meeting) => ((meeting.modules || {}).prompts || {}).enabled !== false
      );

      const docDictionary = await getDocRevisions(meetingsToPrompt);

      // Scan through active speaker events in all running bos (for each
      // meeting that has the prompt module enabled) to see whether they
      // need prompts
      return await checkActiveSpeakers(meetingsToPrompt, docDictionary);
    } catch (error) {
      functions.logger.error(error);
    }
  });

/**
 * Get meetings which requires prompt calculations
 * @param {*} meetings Meeting snapshot in Firebase
 */
function getRunningMeetings(meetings) {
  const uidKeys = Object.keys(meetings);
  const runningMeetings = [];

  // Loop through all uids under the meeting node
  uidKeys.forEach((uid) => {
    const uidMeetings = meetings[uid];
    const meetingKeys = Object.keys(uidMeetings);

    // If the meeting is currently running, add to the meeting list
    meetingKeys.forEach((meetingKey) => {
      let meeting = uidMeetings[meetingKey];
      meeting.meetingNo = meetingKey;
      meeting.uid = uid;
      const actualStartTime = meeting.actualStartTime;
      const actualEndTime = meeting.actualEndTime;

      if (actualStartTime && !actualEndTime) runningMeetings.push(meeting);
    });
  });

  return runningMeetings;
}

/**
 * Get all the doc revisions for the running meetings
 * @param {*} runningMeetings All running meetings in ZoomSense
 */
async function getDocRevisions(runningMeetings) {
  // Get the document snapshots for running meetings
  const promises = [];
  runningMeetings.forEach((meeting) => {
    promises.push(docRef.child(meeting.meetingNo).once("value"));
  });
  const docSnaps = await Promise.all(promises);

  const docDictionary = {};
  docSnaps.forEach((snapshot) => {
    const meetingid = snapshot.key;
    const sensors = snapshot.val();
    docDictionary[meetingid] = sensors;
  });
  return docDictionary;
}

/**
 * Check the active speaker events in all running meetings
 * @param {*} runningMeetings All running meetings in ZoomSense
 */
async function checkActiveSpeakers(runningMeetings, docDictionary) {
  const promises = [];
  let promptPromises = [];

  runningMeetings.forEach((meeting) => {
    promises.push(activeSpeakerRef.child(meeting.meetingNo).once("value"));
  });
  const activeSpeakerSnaps = await Promise.all(promises);
  // functions.logger.info("Active Speaker Snapshots: ", activeSpeakerSnaps);

  activeSpeakerSnaps.forEach((snapshot) => {
    const meetingid = snapshot.key;
    const sensors = snapshot.val();

    // Get the uid of the current meeting
    let uid = "";
    let currentMeeting = {};
    for (let i = 0; i < runningMeetings.length; i++) {
      const meeting = runningMeetings[i];
      if (meeting.meetingNo === meetingid) {
        uid = meeting.uid;
        currentMeeting = meeting;
        break;
      }
    }

    if (sensors) {
      const sensorKeys = Object.keys(sensors);
      const docSensors = docDictionary[meetingid];

      sensorKeys.forEach((sensorKey) => {
        const sensorDetails = sensors[sensorKey];
        // functions.logger.info("Sensor Details: ", sensorDetails);
        let docSensorDetails;
        if (docSensors) docSensorDetails = docSensors[sensorKey];
        // functions.logger.info("Doc Sensor Details: ", docSensorDetails);

        const currentNode = sensorDetails["current"];
        // functions.logger.info("Current Session: ", currentNode);
        if (!currentNode) return;

        // Check if it is the manager sensor and push the host/co-host to the meeting node
        if (!currentNode.isInBO && sensorKey === "ZoomSensor_1") {
          const userList = currentNode.userList;
          if (userList) {
            userIdList = Object.keys(userList);
            userIdList.forEach((id) => {
              const userRole = userList[id].userRole;
              const username = userList[id].username;
              if (userRole === "Host")
                promptPromises.push(
                  meetingRef
                    .child(`${uid}/${meetingid}/host/${username}`)
                    .set({ zoomid: id })
                );
              else if (userRole === "Co-host")
                promptPromises.push(
                  meetingRef
                    .child(`${uid}/${meetingid}/coHost/${username}`)
                    .set({ zoomid: id })
                );
            });
          }
        }

        if (!currentNode.isInBO || !currentNode.activeHistory) return;
        else {
          const activeHistory = Object.values(currentNode.activeHistory);
          const userList = currentNode.userList;
          const promptSent = currentNode.promptSent;
          const { userDurationDict, silencePercentage, startTime } =
            calculatePromptIndicator(
              activeHistory,
              userList,
              getDataLookbackWindowSafe(currentMeeting)
            );
          const docProgress = getDocProgress(
            docSensorDetails,
            getDataLookbackWindowSafe(currentMeeting)
          );
          promptPromises = [
            ...getPromptPromises(
              userDurationDict,
              silencePercentage,
              promptSent,
              userList,
              meetingid,
              sensorKey,
              docProgress,
              startTime,
              currentMeeting
            ),
          ];
        }
      });
    }
  });

  await Promise.all(promptPromises);
}

/**
 * Calculate the active speakering percentage details for breakout rooms
 * @param {*} activeHistory Active speaker event history
 * @param {*} userList Participant list in the current breakout room
 * @param {number} lookbackWindow the duration (in minutes) to look back
 *                                 when calculating speaking percentage
 */
function calculatePromptIndicator(activeHistory, userList, lookbackWindow) {
  let userDict = {};
  let startTime;

  if (activeHistory.length > 0) {
    const currentTime = new Date().getTime();
    startTime = activeHistory[0].timestamp;
    for (let i = 0; i < activeHistory.length - 1; i++) {
      if (
        Array.isArray(activeHistory[i].zoomid) &&
        activeHistory[i].zoomid.length
      ) {
        for (let j = 0; j < activeHistory[i].zoomid.length; j++) {
          let uid = activeHistory[i].zoomid[j];

          // User not in dict already
          if (!userDict[uid]) userDict[uid] = 0;

          // Otherwise, add the active duration
          if (
            currentTime - activeHistory[i].timestamp <=
            lookbackWindow * 60 * 1000
          )
            userDict[uid] +=
              activeHistory[i + 1].timestamp - activeHistory[i].timestamp;
        }
      } else {
        // Calculate silence duration
        if (!userDict) userDict["__Silence"] = 0;
        if (!userDict["__Silence"]) userDict["__Silence"] = 0;

        if (
          currentTime - activeHistory[i].timestamp <=
          lookbackWindow * 60 * 1000
        )
          userDict["__Silence"] +=
            activeHistory[i + 1].timestamp - activeHistory[i].timestamp;
      }
    }
  }

  if (!userList)
    return {
      userDurationDict: null,
      silencePercentage: 0,
      startTime: startTime,
    };

  let names = Object.keys(userDict);
  names = names.sort((n, m) => {
    let a = userList[n] ? userList[n].username : "__Silence";
    let b = userList[m] ? userList[m].username : "__Silence";
    if (a < b) return -1;
    if (a > b) return 1;

    return 0;
  });

  let sumDuration = 0;
  let silenceDuration = 0;
  let userDurationDict = {};
  let usernameList = [];
  let useridList = [];

  for (let zoomid of names) {
    const zoomName = userList[zoomid] ? userList[zoomid].username : "Silence";

    const label = userList[zoomid] ? zoomid : "__Silence";
    const duration = userDict[zoomid] / 1000 / 60;
    if (label !== "__Silence") {
      if (!usernameList.includes(zoomName)) {
        usernameList.push(zoomName);
        useridList.push(label);
        userDurationDict[label] = duration;
      } else {
        const idIndex = usernameList.indexOf(zoomName);
        userDurationDict[useridList[idIndex]] += duration;
      }
      sumDuration += duration;
    } else silenceDuration += duration;
  }

  if (sumDuration !== 0) {
    let labels = Object.keys(userDurationDict);
    labels.forEach((label) => {
      userDurationDict[label] = userDurationDict[label] / sumDuration;
    });
  }
  const silencePercentage =
    sumDuration !== 0 ? silenceDuration / (silenceDuration + sumDuration) : -1;

  functions.logger.info("User Duration Dict: ", userDurationDict);
  functions.logger.info("Silence Percentage: ", silencePercentage);
  functions.logger.info("Breakout Room Start Time: ", startTime);
  return {
    userDurationDict: userDurationDict,
    silencePercentage: silencePercentage,
    startTime: startTime,
  };
}

/**
 * Get the document editing progress for a specific breakout room
 * @param {*} docSensorDetails Document progress for a specific breakout room
 * @param {number} lookbackWindow The number of minutes into the past to consider
 *                                when collecting document response
 */
function getDocProgress(docSensorDetails, lookbackWindow) {
  const docProgress = [];
  if (!docSensorDetails) return docProgress;

  let groupCopy;
  Object.keys(docSensorDetails).forEach((docId) => {
    if (docId !== "driveFolderId") groupCopy = docSensorDetails[docId];
  });

  if (!groupCopy) return docProgress;
  let revisionIds = groupCopy.revisions ? Object.keys(groupCopy.revisions) : [];

  if (revisionIds.length > 0) {
    const lastRevision =
      groupCopy.revisions[revisionIds[revisionIds.length - 1]];

    // Get the time diff for the modification time and the current time
    // If more than 10 minutes, then consider the situation as no doc editing events
    const modifiedAt = new Date(lastRevision.modifiedAt).getTime();
    const currentTime = new Date().getTime();
    const timeDiff = currentTime - modifiedAt;
    // functions.logger.info("Modification Time Diff: ", timeDiff);

    if (timeDiff < lookbackWindow * 60 * 1000) {
      if (lastRevision.responses) {
        for (let i = 0; i < lastRevision.responses.length; i++) {
          // String is not empty and not just whitespace
          if (/\S/.test(lastRevision.responses[i])) docProgress.push(true);
          else docProgress.push(false);
        }
      }
    }
  }

  return docProgress;
}

/**
 * Based on the active speaking percentage, decide whether the prompt messages will be sent
 * @param {*} userDurationDict User dictionary with active speaking percentage
 * @param {*} silencePercentage Silence percentage for the last 5 minutes
 * @param {*} promptSent Timestamp for the last prompt message being sent
 * @param {*} userList User id and name list for the current breakout room
 * @param {*} meetingid Current meeting id
 * @param {*} sensorKey Sensor key for the current breakout room
 * @param {*} docProgress Document progress for the current breakout room
 * @param {*} startTime Breakout room start time
 * @param {*} currentMeeting Current meeting details
 */
function getPromptPromises(
  userDurationDict,
  silencePercentage,
  promptSent,
  userList,
  meetingid,
  sensorKey,
  docProgress,
  startTime,
  currentMeeting
) {
  let promptPromises = [];

  if (currentMeeting.prompts === false) return [];

  const hosts = currentMeeting.host ? Object.keys(currentMeeting.host) : [];
  const cohosts = currentMeeting.coHost
    ? Object.keys(currentMeeting.coHost)
    : [];

  // If the prompt flag is within 3 minutes, return
  const currentTime = new Date().getTime();
  if (promptSent) {
    const timeDiff = currentTime - promptSent;
    if (timeDiff < getPromptFrequencySafe(currentMeeting) * 60 * 1000)
      return [];
  }

  // If breakout room has been created within 2 minutes, return
  const startDiff = currentTime - startTime;
  if (startDiff <= 120 * 1000) return [];

  // If no user duration dictionary, return
  if (!userDurationDict) return [];

  // If silence detected, check document editing and send prompts accordingly
  if (silencePercentage >= 0.75 || silencePercentage === -1) {
    let promptMessage = "";
    if (docProgress && docProgress.length !== 0 && docProgress.includes(true))
      promptMessage =
        "It’s great to see that you have been working together in the shared document. So everyone knows where you are up to, can someone share with the group what they have been working on?";
    else
      promptMessage =
        "This group is pretty quiet, does everyone know what the task involves? Perhaps someone could start by summarising what you are working on.";

    promptPromises.push(
      chatRef
        .child(`${meetingid}/${sensorKey}/message`)
        .push({ receiver: "0", msg: promptMessage })
    );
  } else {
    const uids = Object.keys(userDurationDict);
    const noOfUsers = uids.length;
    let mostActiveUser;
    let mostActiveFlag = false;
    let mostActivePercentage = 0;

    // Retrieve the most active participant in the breakout room
    uids.forEach((uid) => {
      const duration = userDurationDict[uid];
      if (duration >= mostActivePercentage) {
        mostActivePercentage = duration;
        mostActiveUser = uid;
      }
    });

    // Decide on the prompt message based on the active speaking percentage for each user
    uids.forEach((uid) => {
      const duration = userDurationDict[uid];
      // const percentage = parseFloat(duration * 100).toFixed(2) + "%";
      if (
        // We check to see if the user joined since the last prompt was sent
        Date.now() - (userList[uid].joinedAt || 0) >=
          getPromptFrequencySafe(currentMeeting) * 60 * 1000 &&
        duration <= 1 / noOfUsers / 2
      ) {
        // For students who are not speaking actively, randomize the prompt approach with:
        // 1. Prompt directly to the student
        // 2. Ask the most active student to prompt the student to speak up
        const promptType = mostActiveFlag ? 0 : Math.round(Math.random());
        if (promptType !== 0) mostActiveFlag = true;
        const receiver = promptType === 0 ? uid : mostActiveUser;
        const username = userList[uid].username;
        if (
          !(
            (hosts && hosts.includes(username)) ||
            (cohosts && cohosts.includes(username))
          )
        ) {
          const msg =
            promptType === 0
              ? `You are the quietest in the group. If you are struggling with what to say, try summarising someone else’s thoughts to check you both understand each other.`
              : `It would be great if you can help ${username} to engage in the discussion. Why don’t you ask them what they think about the group conversation so far.`;
          promptPromises.push(
            chatRef
              .child(`${meetingid}/${sensorKey}/message`)
              .push({ receiver: receiver, msg: msg })
          );
        }
      }
      // else if (duration >= 1 / noOfUsers * 2) {
      //     const username = userList[uid].username;
      //     promptPromises.push(chatRef.child(`${meetingid}/${sensorKey}/message`)
      //         .push({ receiver: uid, msg: `It’s great that you are contributing to the discussion, ${username}. Why don’t you find out what others in your group think by asking them a question?` }));
      // }
    });
  }

  // If no prompt message need to be sent
  if (promptPromises.length === 0) {
    if (promptSent)
      promptPromises.push(
        activeSpeakerRef
          .child(`${meetingid}/${sensorKey}/current/promptSent`)
          .set(null)
      );
  }
  // If prompt messages are required to be sent
  else
    promptPromises.push(
      activeSpeakerRef
        .child(`${meetingid}/${sensorKey}/current/promptSent`)
        .set(currentTime)
    );

  return promptPromises;
}
