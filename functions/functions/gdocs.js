const functions = require("firebase-functions");
const admin = require("firebase-admin");
const { google } = require("googleapis");
const moment = require("moment-timezone");
const crypto = require("crypto");
var retry = require("async-retry");
const request = require("request");
const cors = require("cors")({
  origin: "*",
});

const localHost = false;
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const db = admin.database();
const OAUTH_REDIRECT_URI =
  (localHost
    ? environment.localhost_functions_url
    : environment.functions_url) + `googleDocsRedirect`;
const oauth2Client = new google.auth.OAuth2(
  environment.gapi.client_id,
  environment.gapi.client_secret,
  OAUTH_REDIRECT_URI
);

const enterResponseTitleText = "Enter your response into the box below:";

// https://cloud.google.com/apis/design/errors#error_retries
const retryCodes = [503]; // not retried by default

// OAuth docs:
// https://github.com/googleapis/google-api-nodejs-client#oauth2-client

// Starts Oauth sign-in process
// User should be logged into an existing zoomsense account
exports.googleDocsAuth = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    // check user is logged in
    if (
      !req.headers.authorization ||
      !req.headers.authorization.startsWith("Bearer ")
    ) {
      functions.logger.error(
        "No Firebase ID token was passed as a Bearer token in the Authorization header."
      );
      res.status(403).send("Unauthorized");
      return;
    }

    var idToken = req.headers.authorization.split("Bearer ")[1];

    admin
      .auth()
      .verifyIdToken(idToken)
      .then((decodedClaims) => {
        var returnUrl = req.query.return || environment.site_url;

        const authUrl = oauth2Client.generateAuthUrl({
          // offline gets refresh_token
          access_type: "offline",
          scope: [
            "https://www.googleapis.com/auth/documents",
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/drive",
          ],
          state: decodedClaims.uid + "!RETURN!" + returnUrl,
          redirect_uri: OAUTH_REDIRECT_URI,
        });

        return res.jsonp(authUrl);
      })
      .catch((error) => {
        functions.logger.info(error);
        return res.status(403).send("Firebase authentication error: " + error);
      });
  });
});

exports.googleDocsRedirect = functions.https.onRequest(async (req, res) => {
  try {
    var stateData = req.query.state.split("!RETURN!");
    var userId = stateData[0];
    var returnUrl = stateData.length < 2 ? environment.site_url : stateData[1];

    const { tokens } = await oauth2Client.getToken(req.query.code);

    if (tokens.refresh_token === null) {
      functions.logger.error("No refresh token");
      return res.redirect(returnUrl);
    }

    oauth2Client.setCredentials(tokens);

    var profile = await google.people("v1").people.get({
      resourceName: "people/me",
      personFields: "emailAddresses,names,photos",
      key: environment.gapi.api_key,
      auth: oauth2Client,
    });

    profile = profile.data;
    var googId = profile.emailAddresses[0].metadata.source.id;

    var toStore = {
      email: profile.emailAddresses[0].value,
      picture: profile.photos[0].url,
      name: profile.names[0].displayName,
      token: tokens.refresh_token,
    };

    // Store account and token details
    await db.ref(`/gdocsTokens/${userId}/${googId}`).update(toStore);
    functions.logger.info("Redirecting to", returnUrl);

    return res.redirect(returnUrl);
  } catch (err) {
    functions.logger.error(err);
    return res.redirect(environment.site_url);
  }
});

async function GetOrCreateFolder(driveApi, rtdbAddress, foldername, parent) {
  try {
    // check if we have a folder ID already stored
    var folderIdSnapshot = await db.ref(rtdbAddress).once("value");

    if (folderIdSnapshot.exists()) {
      var thisId = folderIdSnapshot.val();

      // check that it still exists
      var driveFolder = await driveApi.files.get({ fileId: thisId });

      if (driveFolder) {
        return thisId;
      }
    }

    // no valid folder exists on the user's drive - create it and store the new id
    var fileMetadata = {
      name: foldername,
      mimeType: "application/vnd.google-apps.folder",
    };

    if (parent) fileMetadata.parents = [parent];

    var newFolder = await driveApi.files.create({
      resource: fileMetadata,
      fields: "id",
    });

    await db.ref(rtdbAddress).set(newFolder.data.id);

    return newFolder.data.id;
  } catch (error) {
    functions.logger.error(error);
    return null;
  }
}

async function GetOrCreateRootDocumentsFolder(driveApi, userId, googId) {
  return await GetOrCreateFolder(
    driveApi,
    `/gdocsTokens/${userId}/${googId}/driveFolderId`,
    "ZoomSense Documents",
    null
  );
}

exports.googleDocsDeleteDocument = functions.https.onRequest(
  async (req, res) => {
    return cors(req, res, async () => {
      try {
        // check user is logged in
        if (
          !req.headers.authorization ||
          !req.headers.authorization.startsWith("Bearer ")
        ) {
          functions.logger.error(
            "No Firebase ID token was passed as a Bearer token in the Authorization header."
          );
          return res.status(403).send("Unauthorized");
        }

        const meetingId = req.body.meetingId;
        if (meetingId === null || meetingId.trim() === "") {
          functions.logger.error("No meeting ID passed");
          return res.status(400).send("No meeting ID passed");
        }

        const docId = req.body.documentId;
        if (docId === null || docId.trim() === "") {
          functions.logger.error("No document ID passed");
          return res.status(400).send("No document ID passed");
        }

        var idToken = req.headers.authorization.split("Bearer ")[1];
        var userId = (await admin.auth().verifyIdToken(idToken)).uid;

        if (!userId) {
          return res.status(403).send("Auth error");
        }

        var docSnapshot = await db
          .ref(`/meetings/${userId}/${meetingId}/gdocs/documents/${docId}`)
          .once("value");

        if (!docSnapshot.exists()) {
          functions.logger.error("Document not found for this user");
          throw new Error("Document not found for this user");
        }

        var googleAccount = docSnapshot.val().googleAccount;
        var googleAccSnapshot = await db
          .ref(`gdocsTokens/${userId}/${googleAccount}`)
          .once("value");

        if (!googleAccSnapshot.exists()) {
          throw new Error("Google account not found");
        }

        oauth2Client.setCredentials({
          refresh_token: googleAccSnapshot.val().token,
        });
        var driveApi = google.drive({ version: "v3", auth: oauth2Client });

        await driveApi.files.delete({ fileId: docId });

        await db
          .ref(`/meetings/${userId}/${meetingId}/gdocs/documents/${docId}`)
          .remove();

        return res.status(200).send();
      } catch (error) {
        functions.logger.info(error);
        return res.status(403).send(error);
      }
    });
  }
);

exports.googleDocsAddOrUpdateDocument = functions.https.onRequest(
  async (req, res) => {
    return cors(req, res, async () => {
      // check user is logged in
      if (
        !req.headers.authorization ||
        !req.headers.authorization.startsWith("Bearer ")
      ) {
        functions.logger.error(
          "No Firebase ID token was passed as a Bearer token in the Authorization header."
        );
        return res.status(403).send("Unauthorized");
      }

      var idToken = req.headers.authorization.split("Bearer ")[1];

      const meetingId = req.body.meetingId;
      if (meetingId === null || meetingId.trim() === "") {
        return res.status(400).send("No meeting ID passed");
      }

      const doc = req.body.doc;
      if (doc === null) {
        return res.status(400).send("No document passed");
      }

      if (!doc.sections) doc.sections = [];

      doc.distributed = false;

      var userId = null;
      var gdocsApi = null;
      var driveApi = null;
      var docId = req.body.documentId; //optional, passed if editing existing doc
      var meeting = null;
      var editing = docId ? true : false;

      admin
        .auth()
        .verifyIdToken(idToken)
        .then((decodedClaims) => {
          userId = decodedClaims.uid;
          return db.ref(`/meetings/${userId}/${meetingId}`).once("value");
        })
        .then((meetingSnapshot) => {
          if (!meetingSnapshot.exists()) {
            return res.status(404).send("Meeting not found for this user");
          }

          meeting = meetingSnapshot.val();

          return db
            .ref(`gdocsTokens/${userId}/${doc.googleAccount}`)
            .once("value");
        })
        .then((googleAccSnapshot) => {
          if (!googleAccSnapshot.exists()) {
            return res.status(403).send("Google account not found");
          }

          oauth2Client.setCredentials({
            refresh_token: googleAccSnapshot.val().token,
          });
          gdocsApi = google.docs({ version: "v1", auth: oauth2Client });
          driveApi = google.drive({ version: "v3", auth: oauth2Client });

          if (docId) {
            // document title can't be changed through Docs API for reasons,
            // so have to do a seperate call to Drive
            driveApi.files.update({
              fileId: docId,
              resource: { name: doc.title },
            });
            return GetWipeDocumentContentsRequest(gdocsApi, docId);
          } else {
            return gdocsApi.documents.create({ title: doc.title });
          }
        })
        .then((result) => {
          var docRequests = [];

          if (!docId) {
            // this is a new document, "result" is the result
            // of the document creation request
            docId = result.data.documentId;
          } else if (result) {
            // we're editing - "result" is a delete content request
            // add the delete request so that it's performed first
            functions.logger.info(result);
            docRequests.push(result);
          }

          // loop backwards so that we don't have to faff around with getting the right index to write to
          // i.e. write the document from the bottom up
          for (var i = doc.sections.length - 1; i >= 0; i--) {
            doc.sections[i] = { data: doc.sections[i].data };

            if (doc.sections[i].data.response) {
              // add section's response area to doc
              docRequests.push([
                {
                  insertTable: {
                    rows: 1,
                    columns: 1,
                    location: {
                      index: 1,
                    },
                  },
                },
                {
                  updateTableRowStyle: {
                    tableStartLocation: { index: 2 },
                    rowIndices: [],
                    tableRowStyle: {
                      minRowHeight: {
                        magnitude: 250,
                        unit: "PT",
                      },
                    },
                    fields: "*",
                  },
                },
                {
                  createNamedRange: {
                    name: `zoomsense_section_${i}`,
                    range: {
                      startIndex: 4,
                      endIndex: 6,
                    },
                  },
                },
                {
                  insertText: {
                    location: {
                      index: 1,
                    },
                    text: enterResponseTitleText,
                  },
                },
              ]);
            }

            // add section's content (e.g. activity instructions) to doc
            var content = doc.sections[i].data.content;

            // if null or undefined, make empty string
            if (!content) {
              content = "";
            }

            docRequests.push([
              {
                insertText: {
                  location: {
                    index: 1,
                  },
                  text: "\n" + content + "\n",
                },
              },
              {
                updateParagraphStyle: {
                  paragraphStyle: {
                    namedStyleType: "NORMAL_TEXT",
                  },
                  range: {
                    startIndex: 1,
                    endIndex: content.length + 2,
                  },
                  fields: "namedStyleType",
                },
              },
            ]);

            // add section's title to doc
            docRequests.push([
              {
                insertText: {
                  location: {
                    index: 1,
                  },
                  text: doc.sections[i].data.title,
                },
              },
              {
                updateParagraphStyle: {
                  paragraphStyle: {
                    namedStyleType: "HEADING_2",
                    spaceAbove: {
                      magnitude: 10.0,
                      unit: "PT",
                    },
                    spaceBelow: {
                      magnitude: 10.0,
                      unit: "PT",
                    },
                  },
                  range: {
                    startIndex: 2,
                    endIndex: 2,
                  },
                  fields: "namedStyleType,spaceAbove,spaceBelow",
                },
              },
            ]);

            // if there's a section above this in the document put in a page break
            if (i >= 1 && doc.sections[i - 1].data.response) {
              docRequests.push([
                {
                  insertSectionBreak: {
                    sectionType: "NEXT_PAGE",
                    location: {
                      index: 1,
                    },
                  },
                },
                {
                  insertText: {
                    location: {
                      index: 1,
                    },
                    text: "\n",
                  },
                },
                {
                  updateParagraphStyle: {
                    paragraphStyle: {
                      namedStyleType: "NORMAL_TEXT",
                    },
                    range: {
                      startIndex: 1,
                      endIndex: 1,
                    },
                    fields: "namedStyleType",
                  },
                },
              ]);
            }
          }

          return gdocsApi.documents.batchUpdate({
            documentId: docId,
            resource: { requests: docRequests },
          });
        })
        .then(async () => {
          if (!editing) {
            // google docs api doesn't let you specifiy a parent folder when creating, so move it afterwards
            // existing documents should already be in place
            // if not, who cares - we access by ID not filepath anyway. Let the user move it if they want to.
            var rootFolderId = await GetOrCreateRootDocumentsFolder(
              driveApi,
              userId,
              doc.googleAccount
            );
            var foldername = `${meeting.topic} - ${moment(
              meeting.startTime
            ).format("Do MMM YYYY, HH:mm")}`;
            var parentFolderId = await GetOrCreateFolder(
              driveApi,
              `/meetings/${userId}/${meetingId}/gdocs/driveFolderId`,
              foldername,
              rootFolderId
            );

            // get current folder ids in order to remove them
            var current = await driveApi.files.get({
              fileId: docId,
              fields: "parents",
            });
            var currentParents = current.data.parents.join(",");
            // remove current parent folders and assign the proper one
            await driveApi.files.update({
              fileId: docId,
              addParents: parentFolderId,
              removeParents: currentParents,
              fields: "id",
            });
          }

          db.ref(
            `/meetings/${userId}/${meetingId}/gdocs/documents/${docId}`
          ).set(doc);

          return res.status(200).send();
        })
        .catch((error) => {
          functions.logger.info(error);
          return res.status(403).send(error);
        });
    });
  }
);

// wiping a doc's content requires knowing the start and end index of the content
// 1) get content 2) get end index 3) delete using deleteContentRange
async function GetWipeDocumentContentsRequest(gdocsApi, docId) {
  return gdocsApi.documents
    .get({ documentId: docId })
    .then((docsResp) => {
      var content = docsResp.data.body.content;

      if (content && content.length > 1) {
        // not allowed to delete the final newline character
        var endIndex = content[content.length - 2].endIndex;

        // return to send as part of batch update
        return {
          deleteContentRange: {
            range: {
              startIndex: 1,
              endIndex: endIndex,
            },
          },
        };
      } else return null;
    })
    .catch((err) => {
      functions.logger.error(err);
      return null;
    });
}

/**
 * Given a `data/activeSpeakers/{room_id}/{sensor_id}/history` object,
 * return the same object, but only with entries for "main rooms",
 * i.e. not breakout rooms
 */
const filterBoRoomsFromHistory = (history) => {
  const result = {};
  for (const historyKey in history) {
    if (!history[historyKey].isInBO) {
      result[historyKey] = history[historyKey];
    }
  }
  return result;
};

/**
 * If a user joins a breakout room late, we want to
 * 'redistribute' the google docs for that breakout room
 * (if there are any) to the late joiner
 */
exports.googleDocsRedistributeToLateJoiners = functions.database
  .ref(
    "/data/activeSpeakers/{meetingId}/{sensorId}/current/userList/{userNumber}"
  )
  .onCreate(async (snapshot, context) => {
    const newUser = snapshot.val();
    const activeSpeakersNode = (
      await db
        .ref(`/data/activeSpeakers/${context.params.meetingId}`)
        .once("value")
    ).val();
    const currentRoomSpeakersNode = activeSpeakersNode[context.params.sensorId];
    // If we are in the main room, or the user role is a host, return early
    if (
      !currentRoomSpeakersNode.current.isInBO ||
      newUser.userRole !== "Attendee"
    ) {
      return;
    }
    const allMainRoomStartTimeStamps = Object.values(activeSpeakersNode)
      .map((sensorData) =>
        Object.keys(filterBoRoomsFromHistory(sensorData.history || {}))
      )
      .reduce((acc, el) => acc.concat(el), []);

    // We know that we are in a BO Room, that means that the following
    // is the start time of the most recent main room
    const startTimeOfLastMainRoom = Math.max(
      ...allMainRoomStartTimeStamps.map((val) => parseInt(val))
    );
    const googleDocs = ((
      await db.ref(`/data/gdocs/${context.params.meetingId}`).once("value")
    ).val() || {})[context.params.sensorId];
    if (googleDocs === undefined) {
      return;
    }
    const messagesRef = db.ref(
      `/data/chats/${context.params.meetingId}/${context.params.sensorId}/message`
    );
    for (const docId in googleDocs) {
      if (googleDocs[docId].distributed > startTimeOfLastMainRoom) {
        const message = `A document has been made available to your room. Go to https://docs.google.com/document/d/${googleDocs[docId].driveCopyId}/edit# to view and edit it.`;
        messagesRef.push({ msg: message, receiver: snapshot.key });
      }
    }
  });

exports.googleDocsDistributeAndStartWatchingCopies = functions.https.onRequest(
  async (req, res) => {
    return cors(req, res, async () => {
      try {
        if (
          !req.headers.authorization ||
          !req.headers.authorization.startsWith("Bearer ")
        ) {
          functions.logger.error(
            "No Firebase ID token was passed as a Bearer token in the Authorization header."
          );
          return res.status(403).send("Unauthorized");
        }

        const documentId = req.body.documentId;
        if (!documentId) {
          console.debug("No document ID passed");
          return res.status(400).send("No document ID passed");
        }

        const meetingId = req.body.meetingId;
        if (!meetingId) {
          console.debug("No meeting ID passed");
          return res.status(400).send("No meeting ID passed");
        }

        const roomIds = req.body.roomIds;
        if (!roomIds || roomIds.length === 0) {
          console.debug("No room IDs passed");
          return res.status(400).send("No room IDs passed");
        }

        var idToken = req.headers.authorization.split("Bearer ")[1];
        var userId = (await admin.auth().verifyIdToken(idToken)).uid;

        if (!userId) {
          return res.status(403).send("Auth error");
        }

        const meetingHostId = req.body.hostId ? req.body.hostId : userId;

        // get the meeting from rtdb
        var meeting = await db
          .ref(`meetings/${meetingHostId}/${meetingId}`)
          .once("value");
        if (!meeting.exists()) {
          console.debug("Meeting not found", meetingId);
          return res.status(404).send("Meeting not found");
        }
        meeting = meeting.val();

        // check the document exists
        var document = meeting.gdocs.documents[documentId];
        if (!document) {
          console.debug("Document not found");
          return res.status(404).send("Document not found");
        }

        var googleAcc = await db
          .ref(`gdocsTokens/${meetingHostId}/${document.googleAccount}`)
          .once("value");
        if (!googleAcc.exists()) {
          return res.status(403).send("Google account not found");
        }

        // bots need to be in breakout rooms so that we can distribute the links to the doc's copies
        var chatSnapshot = await db
          .ref(`data/chats/${meetingId}/`)
          .once("value");
        var chats = chatSnapshot.val();

        if (!chats)
          throw new Error("Sensors are not yet all in the breakout rooms");

        oauth2Client.setCredentials({ refresh_token: googleAcc.val().token });
        var driveApi = google.drive({ version: "v3", auth: oauth2Client });

        // get and store the state of the parent document at the time of copying
        // this will be used to see if/how the groups' copies have been edited
        var parentState = await GetResponses(
          oauth2Client,
          document.sections.length,
          documentId
        );
        await db
          .ref(
            `meetings/${meetingHostId}/${meetingId}/gdocs/documents/${documentId}/initialState`
          )
          .set(parentState);

        // prep parent Google Drive folders
        var rootFolderId = await GetOrCreateRootDocumentsFolder(
          driveApi,
          userId,
          document.googleAccount
        );
        var meetingFolderName = `${meeting.topic} - ${moment(
          meeting.startTime
        ).format("Do MMM YYYY, HH:mm")}`;
        var meetingFolderId = await GetOrCreateFolder(
          driveApi,
          `/meetings/${meetingHostId}/${meetingId}/gdocs/driveFolderId`,
          meetingFolderName,
          rootFolderId
        );

        // calculate how long the files should be watched for
        var meetingRemaining = moment.duration(
          moment(meeting.endTime).diff(moment(new Date()))
        );
        var watchDuration = Math.max(meetingRemaining.asMinutes() + 30, 60); // minimum 60 mins

        var promises = [];

        for (var i = 0; i < roomIds.length; i++) {
          if (chats[roomIds[i]].isInBO)
            promises.push(
              CopyAndWatchFile(
                driveApi,
                meetingId,
                roomIds[i],
                meetingFolderId,
                documentId,
                document.title,
                meetingHostId,
                watchDuration
              )
            );
        }

        await Promise.all(promises);

        await db
          .ref(
            `meetings/${meetingHostId}/${meetingId}/gdocs/documents/${documentId}/distributed`
          )
          .set(true);

        return res.status(200).send();
      } catch (error) {
        functions.logger.info(error);
        return res.status(500).send(error);
      }
    });
  }
);

// Create subfolders, copy doc into those folders and then start watching for changes
async function CopyAndWatchFile(
  driveApi,
  meetingId,
  roomId,
  meetingFolderId,
  documentId,
  docTitle,
  userId,
  watchDuration
) {
  return new Promise(async (resolve, reject) => {
    try {
      var subFolderId = await GetOrCreateFolder(
        driveApi,
        `/data/gdocs/${meetingId}/${roomId}/driveFolderId`,
        roomId,
        meetingFolderId
      );

      var copyResult = await driveApi.files.copy({
        fileId: documentId,
        fields: "id",
        resource: {
          parents: [subFolderId],
        },
      });

      await driveApi.permissions.create({
        resource: {
          type: "anyone",
          role: "writer",
        },
        fileId: copyResult.data.id,
        fields: "id",
      });

      await db
        .ref(`/data/gdocs/${meetingId}/${roomId}/${documentId}/driveCopyId`)
        .set(copyResult.data.id);
      await db
        .ref(`/data/gdocs/${meetingId}/${roomId}/${documentId}/distributed`)
        .set(Date.now());

      var chatMessage = `A new document, '${docTitle}', has been made available to your room. Go to https://docs.google.com/document/d/${copyResult.data.id}/edit# to view and edit it.`;

      await db
        .ref(`/data/chats/${meetingId}/${roomId}/message`)
        .push({ msg: chatMessage, receiver: 0 });

      await StartWatchingFile(
        documentId,
        copyResult.data.id,
        meetingId,
        roomId,
        userId,
        watchDuration,
        driveApi
      );
      resolve();
    } catch (error) {
      functions.logger.error(error);
      reject(error);
    }
  });
}

async function StartWatchingFile(
  originalDocId,
  copyId,
  meetingId,
  roomId,
  userId,
  watchMinutes,
  driveApi
) {
  var tokenString = new URLSearchParams([
    ["userId", userId],
    ["meetingId", meetingId],
    ["copyId", copyId],
    ["originalDocId", originalDocId],
    ["roomId", roomId],
  ]);

  var requestBody = {
    kind: "api#channel",
    id: crypto.randomBytes(16).toString("hex"),
    resourceId: copyId,
    token: tokenString.toString(), // arbitrary string that gets returned to web hook
    expiration: moment().add(watchMinutes, "minutes").valueOf(),
    type: "web_hook",
    address: `${environment.functions_url}googleDocsWatchingWebHook/`, // this won't work for localhost
    payload: true,
  };

  // Google API can seemingly return error codes even if our code is right, so set up retries
  // TODO implement this across all API calls
  await retry(
    async (bail) => {
      var response = null;

      try {
        response = await driveApi.files.watch({
          fileId: copyId,
          resource: requestBody,
        });
      } catch (exception) {
        functions.logger.error("exception: " + exception);
        if (response === null) {
          return bail(new Error("Not found"));
        }
      }

      if (403 === response.code) {
        // don't retry upon 403
        // TODO handle 403 'limit exceeded'
        bail(new Error("Unauthorized"));
        return response;
      }

      if (retryCodes.includes(response.code)) {
        throw new Error("returned: " + response.code);
      }

      return response;
    },
    {
      retries: 5,
      onRetry: (err) => {
        functions.logger.info("retrying: " + err);
      },
    }
  );
}

// handle text and inline content
function readParagraphElement(document, element) {
  if (element.textRun) {
    // standard text
    return element.textRun.content;
  }
  if (element.inlineObjectElement) {
    var objId = element.inlineObjectElement.inlineObjectId;
    var imgTag = "\n[img]404[/img]";

    try {
      var embeddedObj =
        document.data.inlineObjects[objId].inlineObjectProperties
          .embeddedObject;
      if (embeddedObj.imageProperties) {
        // this is an image
        imgTag = `[img]${embeddedObj.imageProperties.contentUri}[/img]`;
      } else if (embeddedObj.embeddedDrawingProperties) {
        // this is a shape/drawing
        // can't find any way to meaningfully reference them externally,
        // so storing the ID in case we can do it later
        imgTag = `[drawing]${objId}[/drawing]`;
      }
    } catch (exception) {
      functions.logger.info(exception);
    }

    return imgTag;
  }

  return "[unknown]";
}

// https://developers.google.com/docs/api/samples/extract-text
function readStructuralElementsRecursively(document, elements) {
  var text = "";
  elements.forEach((element) => {
    if (element.paragraph) {
      element.paragraph.elements.forEach((elem) => {
        text += readParagraphElement(document, elem);
      });
    } else if (element.table) {
      // The text in table cells are in nested Structural Elements, so this is recursive
      text += "[table]";
      element.table.tableRows.forEach((row) => {
        text += "[row]";
        row.tableCells.forEach((cell) => {
          text += `[cell]${readStructuralElementsRecursively(
            document,
            cell.content
          )}[/cell]`;
        });
        text += "[/row]";
      });
      text += "[/table]";
    }
  });

  return text;
}

async function GetResponses(oauth2Client, numSections, documentId) {
  try {
    var gdocsApi = google.docs({ version: "v1", auth: oauth2Client });

    var document = await gdocsApi.documents.get({ documentId: documentId });

    var ranges = document.data.namedRanges;
    var docContent = document.data.body.content;

    var toStore = [];

    for (var i = 0; i < numSections; i++) {
      var range = ranges[`zoomsense_section_${i}`].namedRanges[0].ranges[0];

      // loop through document contents until we hit the right index
      for (var j = 0; j < docContent.length; j++) {
        if (
          docContent[j].startIndex <= range.startIndex &&
          docContent[j].endIndex >= range.endIndex
        ) {
          // we know that the ranges are inside single table cells
          var sectionContents =
            docContent[j].table.tableRows[0].tableCells[0].content;

          toStore.push(
            readStructuralElementsRecursively(document, sectionContents)
          );
        }
      }
    }

    return toStore;
  } catch (err) {
    functions.logger.error(err);
    return null;
  }
}

// https://developers.google.com/drive/api/v3/push
exports.googleDocsWatchingWebHook = functions.https.onRequest(
  async (req, res) => {
    try {
      // Google will ocassionally send GET requests here to make sure
      // it's still authenticated. Need to return a verification code
      // in the response's header.
      if (req.method === "GET") {
        // verify that we own this domain
        console.debug("verifying domain");

        return res.status(200).send(`<!DOCTYPE html> <html> <head>
             <meta name="google-site-verification" content="${environment.domain_verification}" />> 
             </head> <body> </body> </html>`);
      }

      // If it's a POST request, it should be from Google about a Drive update.
      // For some reason, things are sent in the request's header, not the body
      if (req.headers["x-goog-resource-state"] === "sync") {
        // channel has just been set up
        return res.status(200).send();
      }

      var params = new URLSearchParams(req.headers["x-goog-channel-token"]);
      var userId = params.get("userId");
      var meetingId = params.get("meetingId");
      var originalDocId = params.get("originalDocId"); // the original document this doc was copied from
      var copyId = params.get("copyId"); // the id of the document actually being watched
      var roomId = params.get("roomId");
      var numSections = 0;

      console.debug(
        "received update",
        `user: ${userId}, meeting: ${meetingId}, doc: ${originalDocId}, room: ${roomId}`
      );

      var docSnapshot = await db
        .ref(`meetings/${userId}/${meetingId}/gdocs/documents/${originalDocId}`)
        .once("value");

      if (!docSnapshot.exists()) {
        throw Error("Cannot find document with those details");
      }

      var thisDocument = docSnapshot.val();
      numSections = thisDocument.sections.length;

      // use the document to find the Google account that it was created with
      var googleAccSnapshot = await db
        .ref(`gdocsTokens/${userId}/${thisDocument.googleAccount}`)
        .once("value");

      // use that google account's auth token to start watching for changes to the doc
      var refreshToken = googleAccSnapshot.val().token;

      var pageTokenSnapshot = await db
        .ref(
          `/data/gdocs/${meetingId}/${roomId}/${originalDocId}/nextPageToken`
        )
        .once("value");

      var query = {
        fileId: copyId,
        pageSize: 5,
        fields:
          "nextPageToken, revisions/id, revisions/modifiedTime, revisions/lastModifyingUser/displayName, revisions/lastModifyingUser/emailAddress",
      };

      if (pageTokenSnapshot.exists()) {
        query.pageToken = pageTokenSnapshot.val();
      }

      oauth2Client.setCredentials({ refresh_token: refreshToken });

      var response = await google
        .drive({ version: "v3", auth: oauth2Client })
        .revisions.list(query);

      if (response && response.data && response.data.revisions) {
        var revisions = response.data.revisions;

        for (var i = 0; i < revisions.length; i++) {
          db.ref(
            `/data/gdocs/${meetingId}/${roomId}/${originalDocId}/revisions/${revisions[i].id}`
          ).set({
            modifiedAt: revisions[i].modifiedTime,
            modifiedBy: revisions[i].lastModifyingUser,
          });
        }

        if (response.data.nextPageToken) {
          db.ref(
            `/data/gdocs/${meetingId}/${roomId}/${originalDocId}/nextPageToken`
          ).set(response.data.nextPageToken);
        }

        var revisionId = revisions[revisions.length - 1].id;

        var toStore = await GetResponses(oauth2Client, numSections, copyId);

        await db
          .ref(
            `/data/gdocs/${meetingId}/${roomId}/${originalDocId}/revisions/${revisionId}/responses`
          )
          .set(toStore);
      } else {
        console.debug("no revisions returned", reponse);
      }

      // Google needs webhook to return a confirmation
      return res.status(200).send();
    } catch (err) {
      functions.logger.error(err);
      return res.status(500).send();
    }
  }
);
