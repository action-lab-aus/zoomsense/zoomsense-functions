const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();
const meetingRef = db.ref("meetings");
const webinarTokenRef = db.ref("webinarToken");

/**
 * HTTPS function triggers when notification email is received from Amazon SES
 * for new panelist invites. Content will be parsed by the Lambda Function
 * to get the Meeting ID and Webinar Token for ZoomSensor joining
 */
exports.webinarToken = functions.https.onRequest(async (req, res) => {
  try {
    // Parse the request payload and extract Meeting ID & Webinar Token
    const { meetingId, webinarToken } = req.body;
    functions.logger.info("Meeting ID: ", meetingId);
    functions.logger.info("Webinar Token: ", webinarToken);

    // Set the webinar token for the meeting (will be fetched for recurring meetings when new
    // sessions are scheduled via the web client)
    await webinarTokenRef.child(meetingId).set({ token: webinarToken });

    // Loop through all the currently scheduled webinars to see whether there are any without
    // the token. If find any, remove the `scheduled` flag and set the webinar token
    const meetingSnap = await meetingRef.once("value");
    const meetings = meetingSnap.val();
    const uidKeys = Object.keys(meetings);
    const promises = [];

    uidKeys.forEach(async (uid) => {
      const uidMeetings = meetings[uid];
      const meetingKeys = Object.keys(uidMeetings);
      meetingKeys.forEach((meetingKey) => {
        if (meetingKey.split("_")[0] === meetingId) {
          promises.push(
            meetingRef
              .child(`${uid}/${meetingKey}/webinarToken`)
              .set(webinarToken)
          );
          promises.push(
            meetingRef.child(`${uid}/${meetingKey}/scheduled`).remove()
          );
        }
      });
    });

    await Promise.all(promises);

    res.writeHead(200);
    res.end();
  } catch (error) {
    functions.logger.error(error);
    res.writeHead(400);
    res.end();
  }
});
