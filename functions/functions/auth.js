const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cookieParser = require("cookie-parser");
const crypto = require("crypto");
const simpleOAuth = require("simple-oauth2");
const request = require("request");
const axios = require("axios");
const cors = require("cors")({
  origin: "*",
});
const { getAvailableCapacity } = require("./scheduler");
const { getAnonymousToken } = require("./anonymous");
const { formatDate } = require("./util");
const { auth } = require("firebase-admin");

const localHost = false;
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const db = admin.database();
const OAUTH_SCOPES = "user:read";
const SITE_URL = environment.site_url;
const OAUTH_REDIRECT_URI =
  (localHost
    ? environment.localhost_functions_url
    : environment.functions_url) + `authtokenredirect`;

// https://marketplace.zoom.us/docs/guides/auth/oauth

// Login flow
// 1) Get Auth code from Zoom (requires user auth)
// 2) Auth code gets returns to OAUTH_REDIRECT_URI
// 3) Use code to get access token (lasts 1 hour)
// 4) Access token gets returned to ACCESS_REDIRECT_URI
// 4) Use access token to request user and meeting details from Zoom

// Existing user flow
// 1) Check access token is valid (if yes, go to 3)
// 2) Refresh access token using auth code
// 3) Use access token to request user and meeting details from Zoom

function zoomOAuth2Client() {
  // Zoom OAuth 2 setup
  const credentials = {
    client: {
      id: environment.zoom.client_id,
      secret: environment.zoom.client_secret,
    },
    auth: {
      tokenHost: "https://zoom.us",
      tokenPath: "/oauth/authorize",
    },
  };
  return simpleOAuth.create(credentials);
}

/**
 * Redirects the User to the Zoom authentication consent screen. Also the 'state' cookie is set for later state
 * verification. Requires a host be passed through the returnURL param, which will eventually be sent the firebase ID token
 */
exports.requestZoomAuth = functions.https.onRequest((req, res) => {
  const oauth2 = zoomOAuth2Client();
  var returnUrl = req.query.return;

  if (!returnUrl) {
    functions.logger.info("No return url given, using default");
    returnUrl = environment.site_url;
  }

  cookieParser()(req, res, () => {
    const state = req.cookies.state || crypto.randomBytes(20).toString("hex");
    functions.logger.info("Setting verification state:", state);
    res.cookie("state", state.toString(), {
      maxAge: 3600000,
      secure: true,
      httpOnly: true,
    });
    const redirectUri = oauth2.authorizationCode.authorizeURL({
      redirect_uri: OAUTH_REDIRECT_URI,
      scope: OAUTH_SCOPES,
      state: state + "!RETURN!" + returnUrl,
    });
    functions.logger.info("Redirecting to:", redirectUri);
    res.redirect(redirectUri);
  });
});

/**
 * Exchanges a given Zoom auth code passed in the 'code' URL query parameter for a Firebase auth token.
 * The request also needs to specify a 'state' query parameter which will be checked against the 'state' cookie.
 */
exports.authtokenredirect = functions.https.onRequest((req, res) => {
  try {
    cookieParser()(req, res, () => {
      functions.logger.info("Received verification state:", req.cookies.state);

      var stateData = req.query.state.split("!RETURN!");
      var returnUrl =
        stateData.length < 2 ? environment.site_url : stateData[1];

      functions.logger.info("using return url " + returnUrl);

      functions.logger.info("Received state:", stateData[0]);
      if (!localHost && !req.cookies.state) {
        throw new Error(
          "State cookie not set or expired. Maybe you took too long to authorize. Please try again."
        );
      } else if (!localHost && req.cookies.state !== stateData[0]) {
        throw new Error("State validation failed");
      }

      var zoom_tokens = {
        auth: req.query.code,
      };

      exports
        .getAccessTokenFromAuthCode(req.query.code)
        .then((tokens) => {
          zoom_tokens.refresh = tokens.refresh_token;
          zoom_tokens.access = tokens.access_token;
          zoom_tokens.access_expires = getExpireDate(tokens.expires_in);

          return getZoomUserProfile(tokens.access_token);
        })
        .then((userDetails) => {
          let formattedEmail;
          if (userDetails.email.indexOf(",") >= 0) {
            formattedEmail = userDetails.email.split(",")[0].toLowerCase();
          } else formattedEmail = userDetails.email;

          var googAcc = {
            uid: `zoom:${userDetails.id}`,
            email: formattedEmail,
            displayName: userDetails.first_name + " " + userDetails.last_name,
            photoURL: userDetails.pic_url,
          };

          // Create a Firebase account and get the Custom Auth Token.
          return createFirebaseAccount(googAcc, zoom_tokens);
        })
        .then((firebaseIdToken) => {
          return res.redirect(returnUrl + "#/login?token=" + firebaseIdToken);
        })
        .catch((err) => {
          functions.logger.error("signUpChainErr", err);
          return res.status(500).send({ err: err });
        });
    });
  } catch (error) {
    functions.logger.error("authtokenredirectErr", error);
    return res.jsonp({ error: error.toString });
  }
});

exports.getProfile = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    const idToken = req.body.idToken.toString();
    functions.logger.info("Received token", idToken);

    admin
      .auth()
      .verifyIdToken(idToken)
      .then((decodedClaims) => {
        functions.logger.info("decoded claims", decodedClaims);
        return admin.auth().getUser(decodedClaims.uid);
      })
      .then((userRecord) => {
        // See the UserRecord reference doc for the contents of userRecord.
        functions.logger.info(
          "Successfully fetched user data: ",
          userRecord.toJSON()
        );
        return res.jsonp(userRecord.toJSON());
      })
      .catch((error) => {
        functions.logger.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

exports.getScheduledMeetings = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    const idToken = req.body.idToken.toString();
    let userAccessToken;
    let userZoomMeetings;

    // Verify ZoomSense user
    admin
      .auth()
      .verifyIdToken(idToken)
      // Get a valid zoom access token for this user
      .then((decodedClaims) => {
        return exports.getValidZoomAccessToken(decodedClaims.uid);
      })
      // Use the access token to request their scheduled meetings
      .then((accessToken) => {
        userAccessToken = accessToken;
        return exports.getZoomMeetings(userAccessToken, false);
      })
      .then((meetings) => {
        userZoomMeetings = meetings;
        return exports.getZoomWebinars(userAccessToken);
      })
      // Return the meetings
      .then(async (webinars) => {
        const allWebinars = { webinars: [] };

        if (webinars && webinars.webinars) {
          const promises = [];
          for (let i = 0; i < webinars.webinars.length; i++) {
            const webinar = webinars.webinars[i];
            if (webinar.type === "9" || webinar.type === 9)
              promises.push(
                exports.getMeetingDetails(userAccessToken, webinar.id, true)
              );
            else allWebinars.webinars.push(webinar);
          }

          const results = await Promise.all(promises);

          for (let i = 0; i < results.length; i++) {
            const occurrences = results[i].occurrences;
            for (let j = 0; j < occurrences.length; j++) {
              let webinarClone = Object.assign({}, webinars.webinars[i]);
              webinarClone.start_time = occurrences[j].start_time;
              allWebinars.webinars.push(webinarClone);
            }
          }
        }

        return res.jsonp({ meetings: userZoomMeetings, webinars: allWebinars });
      })
      .catch((error) => {
        functions.logger.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

exports.getMeetingCapacityManual = functions.https.onRequest((req, res) => {
  return cors(req, res, async () => {
    if (!req.body.start_time || !req.body.duration) return res.sendStatus(400);

    try {
      const capacity = await getAvailableCapacity(
        req.body.start_time,
        req.body.duration
      );
      return res.jsonp({ capacity: capacity });
    } catch (error) {
      functions.logger.error(error);
      return res.status(401).send(error);
    }
  });
});

/**
 * Given a user's uid, and the meeting key of
 * one of that user's meetings, update the details
 * of the meeting ('startTime', 'endTime', 'password' and 'topic')
 *
 * This function throws, so it is up to the caller
 * to handle errors
 *
 * @param {string} userId
 * @param {string} meetingKey
 * @param {string} zoomAuthToken
 * @returns Promise<void>
 */
const updateZoomSenseMeetingDetails = async (
  userId,
  meetingKey,
  zoomAuthToken
) => {
  const meetingRef = db.ref(`meetings/${userId}/${meetingKey}`);

  const zoomsenseMeeting = (await meetingRef.once("value")).val();
  // We skip over manually scheduled meetings, because we
  // may not have access to the meeting's details.
  // We also skip over deleted meetings, because we don't want
  // to override when they were deleted
  if (zoomsenseMeeting.scheduledManually === true || zoomsenseMeeting.deleted) {
    return;
  }

  // Get the zoom meeting's details from the Zoom API
  const meetingDetails = await exports.getMeetingDetails(
    zoomAuthToken,
    meetingKey.split("_")[0],
    zoomsenseMeeting.type === "webinar" || zoomsenseMeeting.webinarToken
  );

  // Meetings with type 3 don't have any timing information. We don't allow users
  // to create these types of meetings anymore, but in the past we did, so
  // let's skip over them
  if (meetingDetails.type === 3) {
    return;
  }

  // FIXME: Handle recurring meetings - for now, we just shortcircuit
  if (!meetingDetails.start_time && meetingDetails.occurrences) {
    return;
  }

  const zoomStartTime = new Date(meetingDetails.start_time);
  const zoomEndTime = new Date(
    zoomStartTime.valueOf() + meetingDetails.duration * 60 * 1000
  );

  if (!Number.isNaN(zoomEndTime.valueOf()))
    meetingRef.child("endTime").set(formatDate(zoomEndTime));

  if (meetingDetails.password !== undefined)
    meetingRef.child("password").set(meetingDetails.password);

  if (meetingDetails.topic !== undefined)
    meetingRef.child("topic").set(meetingDetails.topic);

  if (!Number.isNaN(zoomStartTime.valueOf())) {
    const formattedStartTime = formatDate(zoomStartTime);
    // Transform the actual start time of the meeting to a key
    // (Because the start time is part of the key of meetings: `meetingId_startTimeAsKey`)
    const startTimeAsKey = formattedStartTime
      .split(".")[0]
      .replace(/-/g, "")
      .replace(/:/g, "");

    // If the meeting start time has changed, we need to create a
    // new meeting node
    if (zoomsenseMeeting.startTime !== formattedStartTime) {
      const anonymousToken = await getAnonymousToken(
        `${zoomsenseMeeting.id}_${startTimeAsKey}`,
        userId
      );

      const anonymousLink = `${SITE_URL}/#/anonymous?token=${anonymousToken}`;
      db.ref(`meetings/${userId}/${zoomsenseMeeting.id}_${startTimeAsKey}`).set(
        {
          ...(await meetingRef.once("value")).val(),
          startTime: formattedStartTime,
          shareLink: anonymousLink,
        }
      );
      meetingRef.set(null);
    }
  }

  // If all of the details we care about are undefined,
  // we consider the meeting 'deleted'
  if (
    Number.isNaN(zoomStartTime.valueOf()) &&
    Number.isNaN(zoomEndTime.valueOf()) &&
    meetingDetails.password === undefined &&
    meetingDetails.topic === undefined
  ) {
    meetingRef.child("scheduled").set(true);
    meetingRef.child("deleted").set(formatDate(new Date()));
  }
};

/**
 * Given an object containing the current value of all of
 * a users' ZoomSense Meetings, update all upcoming
 * meetings
 *
 * @param {string} userId is of the user
 * @param {any} usersMeetings all of a users meetings
 */
const updateUsersMeetingDetails = async (userId, usersMeetings) => {
  // Get the users zoomsense meetings that start more than one minute from now
  const oneMinuteFromNow = new Date(Date.now() + 60000);
  const promises = [];
  // Get users zoom auth token
  const zoomAuthToken = await exports.getValidZoomAccessToken(userId);
  for (const meetingKey in usersMeetings) {
    const meetingStartTime = new Date(usersMeetings[meetingKey].startTime);
    // If the meeting starts more than one minute from now
    if (meetingStartTime > oneMinuteFromNow) {
      promises.push(
        updateZoomSenseMeetingDetails(userId, meetingKey, zoomAuthToken)
      );
    }
  }
  await Promise.all(promises);
};

/**
 * Iterate through a users upcoming ZoomSense meetings,
 * and update the details of the meeting, so they are up
 * to date (we use Zoom as a source of truth)
 *
 * The details that get updated are:
 *  - 'startTime'
 *  - 'endTime'
 *  - 'password'
 *  - 'topic'
 */
exports.refreshDetailsOfUsersUpcomingMeetings = functions.https.onCall(
  async (_data, context) => {
    try {
      if (!context.auth || !context.auth.uid) {
        throw new Error("Not logged in.");
      }

      const usersMeetingsSnapshot = await db
        .ref(`meetings/${context.auth.uid}`)
        .once("value");

      const usersMeetings = usersMeetingsSnapshot.val();

      await updateUsersMeetingDetails(context.auth.uid, usersMeetings);
      return "Success";
    } catch (err) {
      functions.logger.error(err);
      throw new functions.https.HttpsError(
        "refreshDetailsOfUsersUpcomingMeetings",
        err
      );
    }
  }
);

/**
 * This function refreshes the details of all users' upcoming
 * ZoomSense meetings, so they are up to date with the underlying
 * Zoom meetings.
 *
 * The details that get upddated are:
 *  - 'startTime'
 *  - 'endTime'
 *  - 'password'
 *  - 'topic'
 */
exports.updateAllUsersMeetingDetails = functions.pubsub
  .schedule("every 5 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async () => {
    // Get all zoomsense meetings that start more than one minute from now
    const allMeetings = (await db.ref("meetings").once("value")).val();
    const promises = [];

    const users = await auth().listUsers();

    try {
      for (const userId in allMeetings) {
        promises.push(updateUsersMeetingDetails(userId, allMeetings[userId]));
      }
      await Promise.all(promises);
    } catch (error) {
      functions.logger.error(
        "Error updating all users meeting details, ",
        error
      );
    }
  });

exports.getMeetingPassword = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    if (!req.body.idToken || !req.body.meetingid) return res.sendStatus(400);

    const idToken = req.body.idToken.toString();
    const meetingid = req.body.meetingid.toString();
    const isWebinar = req.body.isWebinar;
    const startTime = req.body.startTime;
    let anonymousLink;
    functions.logger.info("Received token", idToken);
    functions.logger.info("isWebinar", isWebinar);

    // Verify ZoomSense user
    admin
      .auth()
      .verifyIdToken(idToken)
      // Get a valid zoom access token for this user
      .then(async (decodedClaims) => {
        const anonymousToken = await getAnonymousToken(
          `${meetingid}_${startTime}`,
          decodedClaims.uid
        );

        anonymousLink = `${SITE_URL}/#/anonymous?token=${anonymousToken}`;

        return exports.getValidZoomAccessToken(decodedClaims.uid);
      })
      // Use the access token to request their scheduled meetings
      .then((accessToken) => {
        // If it is a webinar, add ZoomSensor_1 as the panelist for the meeting
        if (isWebinar) addWebinarPanelist(accessToken, meetingid);

        return exports.getMeetingDetails(accessToken, meetingid, isWebinar);
      })
      // Return the meeting details
      .then(async (meeting) => {
        // functions.logger.info("Successfully fetched user meeting: ", meeting);
        const capacity = await getAvailableCapacity(
          meeting.start_time,
          meeting.duration
        );

        // functions.logger.info("capacity: ", capacity);
        return res.jsonp({
          password: meeting.password,
          capacity: capacity,
          anonymousLink: anonymousLink,
        });
      })
      .catch((error) => {
        functions.logger.error("token err", error);
        return res.status(401).send("Session token is unavailable or invalid");
      });
  });
});

exports.getAccessTokenFromAuthCode = (authCode) => {
  return new Promise((resolve, reject) => {
    let url =
      "https://zoom.us/oauth/token?grant_type=authorization_code&code=" +
      authCode +
      "&redirect_uri=" +
      OAUTH_REDIRECT_URI;

    request
      .post(url, (error, response, body) => {
        if (error) {
          functions.logger.error("zoom access token error", error);
          reject(error);
        }

        // Parse response to JSON
        var toRet = JSON.parse(body);

        if (toRet.access_token) {
          resolve(toRet);
        } else {
          reject(new Error(`No access token returned, response body: ${body}`));
        }
      })
      .auth(environment.zoom.client_id, environment.zoom.client_secret);
  });
};

/**
 * @param {string} uid uid of the user
 * @returns {Promise<string>}
 */
exports.getValidZoomAccessToken = (uid) => {
  return admin
    .database()
    .ref(`/zoomtokens/${uid}`)
    .once("value")
    .then((tokens) => {
      if (!tokens) {
        throw new Error("No tokens found for user id: ", uid);
      }

      var tokensVal = tokens.val();

      // If we have a comfortably valid (at least 6 seconds left) access token already, return that
      if (
        tokensVal.access &&
        new Date(tokensVal.access_expires - 6000) > Date.now()
      ) {
        return tokensVal.access;
      }

      // Otherwise, refresh access token from zoom
      let url =
        "https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token=" +
        tokensVal.refresh;

      return new Promise((resolve, reject) => {
        request
          .post(url, async (error, response, body) => {
            if (error || JSON.parse(body).error) {
              functions.logger.info(
                `User's zoom token was invalid, loggin user with uid ${uid} out`
              );
              await auth().revokeRefreshTokens(uid);
              return reject(error);
            }

            functions.logger.info("resp", body);

            // Parse response to JSON
            var newTokens = JSON.parse(body);

            if (!newTokens) {
              return reject(new Error("No access token returned"));
            }

            tokensVal.access = newTokens.access_token;
            tokensVal.refresh = newTokens.refresh_token;
            tokensVal.access_expires = getExpireDate(newTokens.expires_in);

            // Store updated token details
            db.ref(`/zoomtokens/${uid}`).set(tokensVal);

            return resolve(tokensVal.access);
          })
          .auth(environment.zoom.client_id, environment.zoom.client_secret);
      });
    });
};

function getZoomUserProfile(accessToken) {
  return new Promise((resolve, reject) => {
    request
      .get("https://api.zoom.us/v2/users/me", (error, response, body) => {
        if (error) {
          functions.logger.error("Zoom API Response Error: ", error);
          reject(error);
        } else {
          body = JSON.parse(body);
          resolve(body);
        }
      })
      .auth(null, null, true, accessToken);
  });
}

exports.getZoomMeetings = (accessToken, includePast = false) => {
  var reqType = includePast ? "scheduled" : "upcoming";

  return new Promise((resolve, reject) => {
    request
      .get(
        `https://api.zoom.us/v2/users/me/meetings?type=${reqType}&page_size=100`,
        (error, response, body) => {
          if (error) {
            functions.logger.error("Zoom API Response Error: ", error);
            reject(error);
          } else {
            resolve(JSON.parse(body));
          }
        }
      )
      .auth(null, null, true, accessToken);
  });
};

exports.getZoomWebinars = (accessToken) => {
  return new Promise((resolve, reject) => {
    request
      .get(
        `https://api.zoom.us/v2/users/me/webinars?page_size=100`,
        (error, response, body) => {
          if (error) {
            functions.logger.error("Zoom API Response Error: ", error);
            reject(error);
          } else {
            resolve(JSON.parse(body));
          }
        }
      )
      .auth(null, null, true, accessToken);
  });
};

/**
 * @param {string} accessToken
 * @param {string} meetingid
 * @param {boolean} isWebinar
 * @returns {Promise<any>}
 */
exports.getMeetingDetails = (accessToken, meetingid, isWebinar) => {
  const requestUrl = isWebinar
    ? `https://api.zoom.us/v2/webinars/${meetingid}`
    : `https://api.zoom.us/v2/meetings/${meetingid}`;
  return new Promise((resolve, reject) => {
    request
      .get(requestUrl, (error, response, body) => {
        if (error) {
          functions.logger.error("Zoom API Response Error: ", error);
          reject(error);
        } else {
          resolve(JSON.parse(body));
        }
      })
      .auth(null, null, true, accessToken);
  });
};

async function addWebinarPanelist(accessToken, webinarId) {
  const requestUrl = `https://api.zoom.us/v2/webinars/${webinarId}/panelists`;
  const panelists = [
    {
      name: "ZoomSensor_1",
      email: environment.zoomsense_email,
    },
  ];

  try {
    const result = await axios({
      method: "post",
      url: requestUrl,
      headers: { Authorization: `Bearer ${accessToken}` },
      data: {
        panelists: panelists,
      },
    });
    functions.logger.info("addWebinarPanelist Result: ", result);
  } catch (error) {
    functions.logger.error(
      "addWebinarPanelist Error: ",
      JSON.stringify(addWebinarPanelist)
    );
  }
}

function getExpireDate(expires_in) {
  return new Date().getTime() + expires_in * 1000;
}

/**
 * Creates a Firebase account with the given user profile and returns a custom auth token allowing
 * signing-in this account.
 * Also saves the accessToken to the datastore at /zoomAccessToken/$uid
 *
 * @returns {Promise<string>} The Firebase custom auth token in a promise.
 */
function createFirebaseAccount(googAcc, zoom_tokens) {
  // Save the access token to the Firebase Realtime Database.
  const databaseTask = admin
    .database()
    .ref(`/zoomtokens/${googAcc.uid}`)
    .set(zoom_tokens);
  // Create or update the user account.
  const userCreationTask = admin
    .auth()
    .updateUser(googAcc.uid, googAcc)
    .catch((error) => {
      // If user does not exists we create it.
      if (error.code === "auth/user-not-found") {
        return admin.auth().createUser(googAcc);
      }
      throw error;
    });
  // Wait for all async task to complete then generate and return a custom auth token.
  return Promise.all([userCreationTask, databaseTask]).then(() => {
    // Create a Firebase custom auth token.
    return admin
      .auth()
      .createCustomToken(googAcc.uid)
      .then((token) => {
        functions.logger.info(
          'Created Custom token for UID "',
          googAcc.uid,
          '" Token:',
          token
        );
        return token;
      });
  });
}
