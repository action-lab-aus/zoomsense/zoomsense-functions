const functions = require("firebase-functions");

// Listens for new chat messages added and verify whether leave command need to be pushed
exports.checkChatMessage = functions.database
  .ref("/data/chats/{meetingid}/{sensorid}/{msgid}")
  .onCreate((snapshot, context) => {
    const newChat = snapshot.val();
    if (!newChat.msg) return null;

    const msg = newChat.msg.toLowerCase();
    const isInBO = newChat.isInBO;

    if (msg.includes("bye") && msg.includes("sensor") && isInBO)
      return snapshot.ref.parent.child("leave").update({ leave: true });
    return null;
  });
