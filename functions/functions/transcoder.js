const functions = require("firebase-functions");
const admin = require("firebase-admin");

const db = admin.database();
const processingRef = db.ref("processing").child("recordings");

// Sets a queue item to process any new meeting recording files that enter the system.
exports.updateProcessingQueue = functions.storage
  .object()
  .onFinalize(async (object) => {
    if (object.name.endsWith(".tag")) {
      const queueitem = {
        fileBucket: object.bucket,
        filePath: object.name,
        created: object.timeCreated,
      };

      await processingRef.push(queueitem);
    }
  });
