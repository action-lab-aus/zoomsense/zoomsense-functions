const moment = require("moment-timezone");

/**
 * Format a date to be in the format we use
 * in the rtdb
 *
 * @param {Date} date
 */
exports.formatDate = (date) =>
  moment(date).utc().format("YYYY-MM-DDTHH:mm:ss.000") + "Z";

/**
 * @param {Date} date
 * @returns {string}
 */
exports.timeAsKey = (date) =>
  exports.formatDate(date).split(".")[0].replace(/-/g, "").replace(/:/g, "");
