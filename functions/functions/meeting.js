const functions = require("firebase-functions");
const admin = require("firebase-admin");
const axios = require("axios");

const { getAvailableCapacity } = require("./scheduler");
const { getAnonymousToken } = require("./anonymous");
const { getValidZoomAccessToken, getMeetingDetails } = require("./auth");
const { timeAsKey } = require("./util");

const environment = functions.config()[process.env.GCLOUD_PROJECT];
const SITE_URL = environment.site_url;
const db = admin.database();

/**
 * @typedef {{
 *  endTime?: string,
 *  id?: number,
 *  noOfBreakoutRooms?: number,
 *  password?: string,
 *  requested?: boolean,
 *  shareLink?: string,
 *  startTime?: string,
 *  topic?: string,
 *  type?: number
 * }} Meeting
 */

/**
 * @typedef {{
 *  prompts: {
 *    enabled: boolean,
 *    promptFrequency?: number,
 *    dataLookbackWindow?: number
 *  }
 * }} Modules
 */

/**
 * This function either returns a valid meeting module config object
 * according to it's input, or throws an appropriate error, in the
 * case that it's input is invalid
 *
 * @param {*} moduleConfig
 * @returns {Modules}
 */
const verifyModuleConfig = (moduleConfig) => {
  if (
    moduleConfig === undefined ||
    moduleConfig.prompts === undefined ||
    typeof moduleConfig.prompts.enabled !== "boolean"
  ) {
    throw new functions.https.HttpsError(
      "invalid-argument",
      "Invalid Module config. Must specify whether each module is enabled"
    );
  }

  const promptConfig = moduleConfig.prompts.enabled
    ? {
        enabled: moduleConfig.prompts.enabled,
        promptFrequency: moduleConfig.prompts.promptFrequency,
        dataLookbackWindow: moduleConfig.prompts.dataLookbackWindow,
      }
    : { enabled: false };

  if (
    promptConfig.enabled &&
    (typeof promptConfig.promptFrequency !== "number" ||
      typeof promptConfig.dataLookbackWindow !== "number")
  ) {
    throw new functions.https.HttpsError(
      "invalid-argument",
      "Invalid Prompt module config. If enabled, must supply promptFrequency and dataLookbackWindow as numbers"
    );
  }

  return { prompts: promptConfig };
};

/**
 * Create a ZoomSense meeting
 */
exports.createZoomSenseMeeting = functions.https.onCall(
  /**
   * @param {{ meetingId: number, startTime: string, duration: number, isWebinar: boolean, noOfBORooms: number, modules: Modules, template: String }} data
   */
  async (data, context) => {
    if (!context.auth) {
      throw new functions.https.HttpsError(
        "permission-denied",
        "You must be authenticated to call createZoomSenseMeeting"
      );
    }

    if (
      typeof data.meetingId !== "number" ||
      typeof data.startTime !== "string" ||
      typeof data.isWebinar !== "boolean" ||
      data.modules === undefined ||
      typeof data.duration !== "number" ||
      typeof data.noOfBORooms !== "number"
    ) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "meetingId, startTime, duration, noOfBORooms, modules, and isWebinar are required parameters"
      );
    }

    // If the given module data is valid,
    // return it, otherwise throw an appropriate HttpsError
    const moduleConfig = verifyModuleConfig(data.modules);

    const zoomToken = await getValidZoomAccessToken(context.auth.uid).catch(
      (e) => {
        throw new functions.https.HttpsError("unknown", e);
      }
    );

    if (data.isWebinar) addWebinarPanelist(zoomToken, data.meetingId);

    const zoomMeeting = await getMeetingDetails(
      zoomToken,
      data.meetingId,
      data.isWebinar
    ).catch((e) => {
      throw new functions.https.HttpsError("unknown", e);
    });

    const startTimeDate = new Date(data.startTime);
    const startTimeAsKey = timeAsKey(startTimeDate);

    const meetingRef = db.ref(
      `meetings/${context.auth.uid}/${data.meetingId}_${startTimeAsKey}`
    );

    if ((await meetingRef.once("value")).exists()) {
      throw new functions.https.HttpsError(
        "already-exists",
        "This meeting already exists"
      );
    }

    const capacity = await getAvailableCapacity(data.startTime, data.duration);

    if (data.noOfBORooms > capacity) {
      throw new functions.https.HttpsError(
        "resource-exhausted",
        `There is not enough capacity for this time slot. Maximum number of breakout rooms for this meeting: ${capacity}`
      );
    }

    const anonymousToken = await getAnonymousToken(
      `${data.meetingId}_${startTimeAsKey}`,
      context.auth.uid
    );

    const anonymousLink = `${SITE_URL}/#/anonymous?token=${anonymousToken}`;

    meetingRef.set({
      topic: zoomMeeting.topic,
      endTime: new Date(
        startTimeDate.valueOf() + data.duration * 60 * 1000
      ).toISOString(),
      id: data.meetingId,
      isWebinar: data.isWebinar,
      noOfBreakoutRooms: data.noOfBORooms,
      password: zoomMeeting.password,
      requested: false,
      shareLink: anonymousLink,
      startTime: startTimeDate.toISOString(),
      type: zoomMeeting.type,
      template: data.template ? data.template : null,
      modules: {
        ...moduleConfig,
      },
    });
  }
);

exports.editZoomSenseMeeting = functions.https.onCall(
  /**
   * @param {{ noOfBORooms: number, meetingKey: string }} data
   */
  async (data, context) => {
    if (!context.auth) {
      throw new functions.https.HttpsError(
        "permission-denied",
        "You must be authenticated to call editZoomSenseMeeting"
      );
    }
    if (data.noOfBORooms === undefined || data.meetingKey === undefined) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "noOfBORooms (number) is a required parameter"
      );
    }

    const meetingRef = db.ref(
      `meetings/${context.auth.uid}/${data.meetingKey}`
    );

    const meetingSnap = await meetingRef.once("value");

    if (!meetingSnap.exists) {
      throw new functions.https.HttpsError(
        "not-found",
        `Couldn't find ZoomSense meeting with key ${data.meetingKey}`
      );
    }

    /** @type {Meeting} */
    const currentMeetingData = meetingSnap.val();

    const meetingDuration =
      (new Date(currentMeetingData.endTime).valueOf() -
        new Date(currentMeetingData.startTime).valueOf()) /
      (60 * 1000);

    const capacity = await getAvailableCapacity(
      currentMeetingData.startTime,
      meetingDuration
    );

    if (capacity + currentMeetingData.noOfBreakoutRooms < data.noOfBORooms) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        `Maximum No. of Breakout Rooms for this meeting: ${
          capacity + currentMeetingData.noOfBreakoutRooms
        }`
      );
    }

    meetingRef.child("noOfBreakoutRooms").set(data.noOfBORooms);
  }
);

async function addWebinarPanelist(accessToken, webinarId) {
  const requestUrl = `https://api.zoom.us/v2/webinars/${webinarId}/panelists`;
  const panelists = [
    {
      name: "ZoomSensor_1",
      email: environment.zoomsense_email,
    },
  ];

  try {
    const result = await axios({
      method: "post",
      url: requestUrl,
      headers: { Authorization: `Bearer ${accessToken}` },
      data: {
        panelists: panelists,
      },
    });
    functions.logger.info("addWebinarPanelist Result: ", result);
  } catch (error) {
    functions.logger.error("addWebinarPanelist Error: ", JSON.stringify(error));
  }
}
