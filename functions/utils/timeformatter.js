const moment = require("moment-timezone");

/**
 * Calculate the milliseconds difference between the scheduled and current date time
 * @param {*} startTime Meeting scheduled date time
 */
function getMsDiff(startTime) {
  return moment(startTime).toDate().getTime() - Date.now();
}

/**
 * Check whether the current meeting to be scheduled overlaps with other scheduled meetings.
 * If `sTime` <= `startTime` < `eTime` or  `sTime` < `startTime + duration` <= `eTime`
 *
 * @param {moment.MomentInput} startTime Start time of the meeting
 * @param {number} duration Meeting duration in minutes
 * @param {moment.MomentInput} sTime Start time of the meeting to be compared with
 * @param {moment.MomentInput} eTime End time of the meeting to be compared with
 */
function isOverlapping(startTime, duration, sTime, eTime) {
  const sDateTime = moment(sTime).toDate();
  const eDateTime = moment(eTime).toDate();
  const startDateTime = moment(startTime).toDate();
  const durationDiff = duration * 60 * 1000;

  if (
    (startDateTime >= sDateTime && startDateTime < eDateTime) ||
    (sDateTime - startDateTime < durationDiff &&
      eDateTime - startDateTime >= durationDiff)
  ) {
    console.log("sDateTime: ", sDateTime);
    console.log("eDateTime: ", eDateTime);
    console.log("startDateTime: ", startDateTime);
    console.log("sDateTime - startDateTime: ", sDateTime - startDateTime);
    console.log("eDateTime - startDateTime: ", eDateTime - startDateTime);
    return true;
  }

  return false;
}

exports.getMsDiff = getMsDiff;
exports.isOverlapping = isOverlapping;
