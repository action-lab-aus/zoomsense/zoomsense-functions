const functions = require("firebase-functions");
const admin = require("firebase-admin");
const environment = functions.config()[process.env.GCLOUD_PROJECT];

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: environment.firebase_credentials.project_id,
    clientEmail: environment.firebase_credentials.client_email,
    privateKey: environment.firebase_credentials.private_key.replace(
      /\\n/g,
      "\n"
    ), // remove any added escape slashes
  }),
  databaseURL: environment.firebase_rtdb,
});

const auth = require("./functions/auth");
const breakoutRoom = require("./functions/breakoutRoom");
const chat = require("./functions/chat");
const scheduler = require("./functions/scheduler");
const transcoder = require("./functions/transcoder");
const prompt = require("./functions/prompt");
const gdocs = require("./functions/gdocs");
const anonymous = require("./functions/anonymous");
const notification = require("./functions/notification");
const meeting = require("./functions/meeting");
// const twitter = require("./functions/twitter");

exports.requestZoomAuth = auth.requestZoomAuth;
exports.authtokenredirect = auth.authtokenredirect;
exports.getProfile = auth.getProfile;
exports.refreshDetailsOfUsersUpcomingMeetings =
  auth.refreshDetailsOfUsersUpcomingMeetings;
exports.updateAllUsersMeetingDetails = auth.updateAllUsersMeetingDetails;
exports.getScheduledMeetings = auth.getScheduledMeetings;
exports.getMeetingPassword = auth.getMeetingPassword;
exports.getMeetingCapacityManual = auth.getMeetingCapacityManual;
exports.onNewHistoryCreate = breakoutRoom.onNewHistoryCreate;
exports.onChatIsInBoUpdate = breakoutRoom.onChatIsInBoUpdate;
exports.checkChatMessage = chat.checkChatMessage;
exports.decryptToken = anonymous.decryptToken;
exports.generateLink = anonymous.generateLink;
exports.zoomScheduler = scheduler.zoomScheduler;
exports.sanityCheck = scheduler.sanityCheck;
exports.leaveMeetingScheduler = scheduler.leaveMeetingScheduler;
exports.rescheduleSensors = scheduler.rescheduleSensors;
exports.updateProcessingQueue = transcoder.updateProcessingQueue;
exports.promptScheduler = prompt.promptScheduler;
exports.googleDocsAuth = gdocs.googleDocsAuth;
exports.googleDocsRedirect = gdocs.googleDocsRedirect;
exports.googleDocsAddOrUpdateDocument = gdocs.googleDocsAddOrUpdateDocument;
exports.googleDocsDeleteDocument = gdocs.googleDocsDeleteDocument;
exports.googleDocsWatchingWebHook = gdocs.googleDocsWatchingWebHook;
exports.googleDocsRedistributeToLateJoiners =
  gdocs.googleDocsRedistributeToLateJoiners;
exports.googleDocsDistributeAndStartWatchingCopies =
  gdocs.googleDocsDistributeAndStartWatchingCopies;
exports.webinarToken = notification.webinarToken;
exports.createZoomSenseMeeting = meeting.createZoomSenseMeeting;
exports.editZoomSenseMeeting = meeting.editZoomSenseMeeting;
// exports.createTwitter = twitter.createTwitter;
