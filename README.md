# ZoomSense - Firebase Cloud Functions

This repository contains the Firebase Cloud Functions which act as the serverless backend for the ZoomSense infrastructure. These functions utilise external APIs, storing information in the project's [**Firebase Realtime Database**](https://firebase.google.com/docs/database) (RTDB), which is [**encrypted at rest**](https://firebase.google.com/support/privacy). It currently consists of 7 main modules, stored under the `/functions/functions` directory:

## 1. Auth

This module creates Firebase accounts for users, using [**Zoom's OAuth API**](https://marketplace.zoom.us/docs/guides/build/oauth-app) as an intermediary. Users' basic Zoom profile information (name, email, avatar) and their authentication tokens are stored in the RTDB. This module also provides other functions related to users' Zoom profiles, such as retrieving a list of their upcoming meetings for scheduling ZoomSense integration.

### Retrieve Scheduled Zoom Meetings

When retrieving the scheduled Zoom meetings, the `page_size` query parameter defines the number of records returned within a single API call (in `auth.js`, `getZoomMeetings` function). The current implementation uses 100 as the page size for making the API call. However, if more scheduled meetings exist under the user's Zoom account, pagination can also be implemented to fulfill the need. For more information, please check [**Zoom List Meetings**](https://marketplace.zoom.us/docs/api-reference/zoom-api/meetings/meetings).

## 2. Scheduler

Manages ZoomSense for the scheduling of Windows Sensors. Features a [**Cloud Pub/Sub Trigger**](https://firebase.google.com/docs/functions/pubsub-events) function which is triggered every minute, and allocates Zoom Sensors to any scheduled meetings which will start within the following 30 minutes.

## 3. Chat

Monitors and reacts to chat messages in Zoom breakout rooms, enabling interactions such as the "_bye bye sensor_" functionality.

## 4. Google Docs

Using the Google [**Docs**](https://developers.google.com/docs/api), [**Drive**](https://developers.google.com/drive) and [**People**](https://developers.google.com/people) API scopes with the [**Node.js Google API client**](https://github.com/googleapis/google-api-nodejs-client), this module supports users in associating their Google account(s) with their existing ZoomSense account. Once authenticated, users can create Google Docs through ZoomSense, which will distribute copies of each document to the break-out rooms during the call. These documents are structured in such a way that supports the system tracking the meeting participants' progress through given activities, such as in a workshop or seminar.

## 5. Transcoder

Sets a queue item to process any new raw meeting recording files that enter the system.

## 6. Anonymous

Supports anonymous authentication features including

- Generate an anonymous sign-in link with encrypted tokens for non-hosts
- Decrypt and validate the token for allowing access

## 7. Prompt

Firebase Cloud Function scheduled at a 1-minute interval to check whether high-level prompt messages need to be sent into breakout rooms by the ZoomSense Windows Sensors when a certain pattern has been identified (e.g. one person is not talking actively during the session).

# ZoomSense - Rules

This repository also contains Firebase Realtime Database rules and Storage rules for secured data access:

- **Realtime Database Rules**: `database.json`
- **Storage Rules**: `storage.rules`

# Building, Testing, and Deploying

## Prerequisites

- [Firebase Project](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md) with **Blaze Plan (Pay as you go)**
- [Zoom OAuth App](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/3_QuickStart_Zoom_OAuth.md)

## 1. Create a Firebase Project

A quick overview of how to create a Firebase Project for ZoomSense with a Realtime Database, Storage, Authentication can be found under the [ZoomSense QuickStart Guide](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md).

## 2. Install Firebase CLI

Running and deploying Firebase services requires the installation of the [**Firebase CLI**](https://firebase.google.com/docs/cli).

Run the following npm command to install the CLI or update to the latest CLI version.

```
npm install -g firebase-tools
```

If using Windows, Git Bash doesn't work - Windows Powershell seems to work best. If you're using Windows Powershell, you will probably have to run the following command each session you want to use the Firebase CLI:

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
```

Run the following command to sign in to Google.

```
firebase login
```

## 3. Generate **Zoom OAuth** Key and Secret

Go to [Zoom MarketPlace](https://marketplace.zoom.us/) and create a Zoom account.

Under the **Develop** tab, select **Build App**. Choose Oauth to start creating an OAuth App on Zoom:

- Provide the OAuth App Name
- Choose the app type to be **User-managed app**
- Deselect publishing to Zoom Marketplace
- Click **Create** to create the OAuth App

### 3.1 Configure Redirect and Whitelist URL for OAuth

You will be provided with Zoom OAuth Credentials for accessing Zoom APIs. Under the **App Credentials** section, fill in the **Redirect URL for OAuth** & **Add Whitelist URLs** with:

```
https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/authtokenredirect
```

The FIREBASE_PROJECT_ID can be found in the Firebase console, under **Project settings**'s **General** section.

### 3.2 Configure Zoom OAuth App Scopes

Leave the default details under the **Information** and **Feature** section, and go to the **Scopes** section. Add the following scopes via the console so that we will be able to retrieve the user and meeting details on behalf of the user:

- `meeting:read`
- `meeting:write`
- `user:read`
- `user_profile`

## 4. Configure Environment Variables for Firebase Functions

Run `firebase use --add` to define the project alias to be used for the Firebase Function (set the alias to be **default**). Find and choose the Firebase Project you have created in Step 1.

Create a `.runtimeconfig.json` file under the root directory of the function repository. Copy the following JSON object into the file and update it with the credentials you received from the previous steps.

- `zoom.client_id` & `zoom.client_secret`: Zoom OAuth App Key and Secret created in Step 3
- `firebase_credentials`: Firebase Admin Service Account
- `gapi`: Credentials can be found under the Credentials sections in [GCP APIs and Services](https://console.developers.google.com)

```JSON
{
    "zoomsense-plugin": {
        "domain_verification": "*******",
        "zoomsense_email": "*******",
        "functions_url": "https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/",
        "localhost_functions_url": "http://localhost:5001/FIREBASE_PROJECT_ID/us-central1/",
        "site_url": "https://FIREBASE_PROJECT_ID.web.app/http://localhost:8080",
        "firebase_rtdb": "https://FIREBASE_DATABASE_URL.firebaseio.com/",
        "zoom": {
            "client_id": "Zoom OAuth App Client ID",
            "client_secret": "Zoom OAuth App Client Secret"
        },
        "firebase_credentials": {
            "type": "service_account",
            "project_id": "*******",
            "private_key_id": "*******",
            "private_key": "-----BEGIN PRIVATE KEY-----********\n-----END PRIVATE KEY-----\n",
            "client_email": "*******@appspot.gserviceaccount.com",
            "client_id": "*******",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/*******%40appspot.gserviceaccount.com"
        },
        "gapi": {
            "client_id": "Google OAuth 2.0 Client ID",
            "client_secret": "Google OAuth 2.0 Client Secret",
            "api_key": "Browser Key"
        },
        "anonymous": {
            "encryption_key": "Encryption Key for Anonymous Sign-in (Any String)",
            "encryption_secret": "Encryption Secret for Anonymous Sign-in (Any String)"
        }
    }
}
```

The name "zoomsense-plugin" in the above configuration is the default project name configured in `.firebaserc`

Set environment configuration for your project by using the `firebase functions:config:set` command in the Firebase CLI. More details can be found on [Environment Configuration](https://firebase.google.com/docs/functions/config-env).

For example, you can run the following command on Mac/Linux to set environment configuration from the `.runtimeconfig.json` file:

```
firebase functions:config:set FIREBASE_PROJECT_ID="$(cat .runtimeconfig.json)"
```

## 5. Run Firebase Functions Locally

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder, make the following changes to run Firebase functions locally.

1. For the Zoom OAuth Client ID & Secret, please use the one with a Redirect URL for OAuth pointing to the local function's endpoint:

<img src="./img/Zoom Local.png" width="600" alt="Zoom Local OAuth App" />

```
"zoom": {
    "client_id": "Zoom OAuth App Client ID",
    "client_secret": "Zoom OAuth App Client Secret"
},
```

2. Update `/functions/functions/auth.js` on Line 16 and change it to `const localHost = true;`.

After making these changes, functions can be tested locally with Zoom OAuth enabled using the following command:

```
npm run emulators
```

## 6. Deploy Firebase Functions & Rules

To deploy the Firebase Function:

```
(All Changes) firebase deploy
(Only Functions and Config Changes) firebase deploy  --only functions
(Config Changes and Specific Functions) firebase deploy --only functions:function1,functions:function2
```
