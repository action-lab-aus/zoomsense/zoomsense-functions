"use strict";

require("dotenv").config();
const AWS = require("aws-sdk");
const simpleParser = require("mailparser").simpleParser;
const axios = require("axios");
const s3 = new AWS.S3({
  apiVersion: "2006-03-01",
  region: "us-east-1",
});

module.exports.parseEmail = async (event) => {
  const record = event.Records[0];

  const request = {
    Bucket: "zoomsense-webinar",
    Key: record.ses.mail.messageId,
  };

  try {
    const data = await s3.getObject(request).promise();
    const email = await simpleParser(data.Body);
    const emailContent = email.text;

    const slug = emailContent.split("https://monash.zoom.us/w/")[1];
    const strArr = slug.split("?tk=");

    const meetingId = strArr[0];
    const webinarToken = strArr[1].split("&pwd=")[0];

    console.log("meetingId: ", meetingId);
    console.log("webinarToken: ", webinarToken);

    const result = await axios.post(process.env.targetURL, {
      meetingId: meetingId,
      webinarToken: webinarToken,
    });
    console.log("Firebase Notification Result: ", result);

    return { status: "success" };
  } catch (Error) {
    console.log(Error, Error.stack);
    return Error;
  }
};
